# Sepia Theme

## Theme
### Documentation 
https://plugins.jetbrains.com/docs/intellij/themes-customize.html
### Example
- \app-client.jar!\themes\HighContrast.theme.json
### Configuration
- JDK.themeMetadata.json
- IntelliJPlatform.themeMetadata.json
### Code
- com.intellij.util.ui.JBUI.CurrentTheme

## Scheme
### Example
- /themes/highContrastScheme.xml
### Configuration
- DefaultColorSchemesManager.xml
### Code
- com.intellij.openapi.editor.colors.EditorColors

## General
- com.intellij.ui.JBColor.namedColor(java.lang.String, java.awt.Color)
- javax.swing.UIManager.getColor(java.lang.Object)
