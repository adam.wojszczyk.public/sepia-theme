package plugin.style.theme;

import plugin.model.Palette;
import plugin.model.color.Color;

import static java.util.Objects.requireNonNull;

public class Foreground {

	private final Palette palette;

	Foreground(Palette palette) {
		this.palette = requireNonNull(palette);
	}

	public Color base() {
		return palette.black().darker(2);
	}

	public Color info() {
		return palette.gray();
	}

	public Color disabled() {
		return palette.gray().brighter();
	}

	public Color modified() {
		return palette.blue();
	}
}
