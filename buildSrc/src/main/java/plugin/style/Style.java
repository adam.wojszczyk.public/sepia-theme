package plugin.style;

import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.scheme.Scheme;
import plugin.style.theme.Theme;

import static java.util.Objects.requireNonNull;

public class Style {

	private final Palette palette;

	public Style(Palette palette) {
		this.palette = requireNonNull(palette);
	}

	public Theme theme() {
		return new Theme(palette);
	}

	public Scheme scheme() {
		return new Scheme(this, palette);
	}

	public ScrollBar scrollBar() {
		return new ScrollBar(theme());
	}

	public Color success() {
		return palette.green();
	}

	public Color error() {
		return palette.red();
	}

	public Color link() {
		return palette.blue();
	}
}
