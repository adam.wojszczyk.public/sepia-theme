package plugin.style.scheme;

import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

public class Changes {

	private final Style style;
	private final Palette palette;

	public Changes(Style style, Palette palette) {
		this.style = requireNonNull(style);
		this.palette = requireNonNull(palette);
	}

	public Color added() {
		return palette.green();
	}

	public Color modified() {
		return palette.blue();
	}

	public Color deleted() {
		return style.scheme().background().base().darker(5);
	}

	public Color addedFilestatus() {
		return palette.aqua();
	}
}
