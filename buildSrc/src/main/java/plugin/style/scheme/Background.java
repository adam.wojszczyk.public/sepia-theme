package plugin.style.scheme;

import plugin.model.Palette;
import plugin.model.color.Color;

import static java.util.Objects.requireNonNull;

public class Background {

	private final Palette palette;

	public Background(Palette palette) {
		this.palette = requireNonNull(palette);
	}

	public Color underCaretWrite() {
		return underCaret().darker();
	}

	public Color inlayHint() {
		return underCaret();
	}

	public Color underCaret() {
		return selectedLine().darker(2);
	}

	public Color selectedText() {
		return palette.blue().brighter();
	}
	public Color selectedLine() {
		return base().darker(2);
	}

	public Color hints() {
		return readOnly();
	}

	public Color readOnly() {
		return base().darker();
	}

	public Color documentation() {
		return base();
	}

	public Color base() {
		return palette.sepia().brightest();
	}
}
