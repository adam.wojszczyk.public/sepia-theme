package plugin.style.scheme;

import plugin.model.Palette;
import plugin.model.color.Color;

import static java.util.Objects.requireNonNull;

public class Foreground {

	private final Palette palette;

	Foreground(Palette palette) {
		this.palette = requireNonNull(palette);
	}

	public Color inlayCurrentHint() {
		return inlayHighlightedHint().darker(2);
	}

	public Color inlayHighlightedHint() {
		return inlayHint().darker();
	}

	public Color inlayHint() {
		return inlayHintWithoutBackground().darker();
	}

	public Color inlayHintWithoutBackground() {
		return disabled().brighter();
	}

	public Color deprecated() {
		return unused();
	}

	public Color disabled() {
		return palette.gray().brighter();
	}

	public Color unused() {
		return palette.gray();
	}

	public Color selectedText() {
		return base();
	}

	public Color base() {
		return palette.black().darker(2);
	}
}
