package plugin.style.scheme;

import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

public class Scheme {

	private final Style style;
	private final Palette palette;

	public Scheme(Style style, Palette palette) {
		this.style = requireNonNull(style);
		this.palette = requireNonNull(palette);
	}

	public Foreground foreground() {
		return new Foreground(palette);
	}

	public Background background() {
		return new Background(palette);
	}

	public Color lines() {
		return palette.gray().brighter(3);
	}

	public Changes changes() {
		return new Changes(style, palette);
	}

	public Color warning() {
		return palette.orange().brighter();
	}

	public Color weak_warning() {
		return style.theme().warning();
	}
}
