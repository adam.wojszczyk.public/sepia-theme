package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class ProgressBar {
	private final Style style;
	ProgressBar(Style style) { this.style = requireNonNull(style);}

	@JsonProperty Color background() { return null; } // unknown effect
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color trackColor() { return style.theme().background().base().darker(2); }
	@JsonProperty Color progressColor() { return trackColor().darker(3); }
	@JsonProperty Color indeterminateStartColor() { return trackColor(); }
	@JsonProperty Color indeterminateEndColor() { return progressColor(); }
	@JsonProperty Color passedColor() { return style.success().brighter(2); }
	@JsonProperty Color passedEndColor() { return trackColor(); }
	@JsonProperty Color failedColor() { return style.error(); }
	@JsonProperty Color failedEndColor() { return trackColor(); }
	@JsonProperty Color selectionBackground() { return null; } // unknown effect
	@JsonProperty Color selectionForeground() { return null; } // unknown effect
}
