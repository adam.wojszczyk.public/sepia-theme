package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Menu {
	private final Style style;
	Menu(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Color acceleratorForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color acceleratorSelectionForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color disabledBackground() { return null; }
	@JsonProperty Color disabledForeground() { return null; }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color separatorColor() { return style.theme().borderColor(); } // * color works as supposed, this one changes color but not exactly as expected
}
