package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class PopupMenu {
	private final Style style;
	PopupMenu(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Integer borderInsets() { return null; }
	@JsonProperty Integer borderWidth() { return 0; }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color translucentBackground() { return background().opacity(0.7); }
}
