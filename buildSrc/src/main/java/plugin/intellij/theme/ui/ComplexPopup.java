package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.Effect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

@Effect("SearchEverywhere Popup")
public class ComplexPopup {
	private final Style style;
	ComplexPopup(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Header Header() { return new Header(style); }

	static class Header {
		private final Style style;
		Header(Style style) {
			this.style = requireNonNull(style);
		}

		@JsonProperty Color background() { return style.theme().background().base().darker(2); }
	}
}
