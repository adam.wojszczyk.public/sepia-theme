package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class SpeedSearch {
	private final Style style;
	SpeedSearch(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color errorForeground() { return style.error(); }
	@JsonProperty Color background() { return style.theme().background().input(); }
	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
}
