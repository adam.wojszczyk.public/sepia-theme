package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class VersionControl {
	private final Palette palette;
	private final Style style;
	VersionControl(Palette palette, Style style) {
		this.palette = requireNonNull(palette);
		this.style = requireNonNull(style);
	}

	@JsonProperty("FileHistory.Commit.selectedBranchBackground") Color selectedBranchBackground() { return null; }
	@JsonProperty GitLog GitLog() { return new GitLog(); }
	@JsonProperty Log Log() { return new Log(style); }
	@JsonProperty MarkerPopup MarkerPopup() { return new MarkerPopup(style); }
	@JsonProperty RefLabel RefLabel() { return new RefLabel(palette, style); }

	static class GitLog {
		@JsonProperty Color headIconColor() { return null; }
		@JsonProperty Color localBranchIconColor() { return null; }
		@JsonProperty Color otherIconColor() { return null; }
		@JsonProperty Color remoteBranchIconColor() { return null; }
		@JsonProperty Color tagIconColor() { return null; }
	}

	/*
		background = UI.this.List().background
		selectionBackground = UI.this.List().selectionBackground
	*/
	static class Log {
		private final Style style;
		Log(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Log.Commit Commit() { return new Log.Commit(style); }

		static class Commit {
			private final Style style;
			Commit(Style style) { this.style = requireNonNull(style); }

			@JsonProperty Color currentBranchBackground() { return style.theme().background().input().darker(); }
			@JsonProperty Color hoveredBackground() { return currentBranchBackground().darker(); }
			@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
			@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
			@JsonProperty Color selectionInactiveForeground() { return style.theme().foreground().base(); }
			@JsonProperty Color selectionInactiveBackground() { return style.theme().background().selectedInactive(); }
			@JsonProperty Color unmatchedForeground() { return null; }
		}
	}

	static class MarkerPopup {
		private final Style style;
		MarkerPopup(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color borderColor() { return style.theme().borderColor(); }
		@JsonProperty MarkerPopup.Toolbar Toolbar() { return new MarkerPopup.Toolbar(style); }

		static class Toolbar {
			private final Style style;
			Toolbar(Style style) { this.style = requireNonNull(style); }

			@JsonProperty Color background() { return style.theme().background().base(); }
		}
	}

	static class RefLabel {
		private final Palette palette;
		private final Style style;
		RefLabel(Palette palette, Style style) {
			this.palette = requireNonNull(palette);
			this.style = requireNonNull(style);
		}

		@JsonProperty Color backgroundBase() { return palette.blue(); }
		@JsonProperty double backgroundBrightness() { return 0.50; }
		@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	}
}
