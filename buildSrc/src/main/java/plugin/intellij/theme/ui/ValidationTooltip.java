package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class ValidationTooltip {
	private final Style style;
	ValidationTooltip(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color errorBackground() { return style.error(); }
	@JsonProperty Color errorBorderColor() { return errorBackground().darker(); }
	@JsonProperty Color warningBackground() { return style.theme().warning().brighter(); }
	@JsonProperty Color warningBorderColor() { return warningBackground().darker(); }
}
