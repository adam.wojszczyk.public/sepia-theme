package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.Effect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

@Effect("project tree")
@Effect("settings tree")
class Tree {
	private final Style style;
	Tree(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color errorForeground() { return style.error(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color hash() { return style.theme().background().base().darker(2); }
	@JsonProperty Color hoverBackground() { return style.theme().background().hover(); }
	@JsonProperty Color hoverInactiveBackground() { return hoverBackground(); }
	@JsonProperty Color modifiedItemForeground() { return style.theme().foreground().modified(); }
	@JsonProperty Boolean paintLines() { return null; }
	@JsonProperty Color rowHeight() { return null; }
	@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionInactiveBackground() { return style.theme().background().selectedInactive(); }
}
