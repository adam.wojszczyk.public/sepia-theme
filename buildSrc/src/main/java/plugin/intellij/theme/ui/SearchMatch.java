package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class SearchMatch {
	private final Style style;
	SearchMatch(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color startBackground() { return style.theme().background().search(); }
	@JsonProperty Color endBackground() { return startBackground(); }
}
