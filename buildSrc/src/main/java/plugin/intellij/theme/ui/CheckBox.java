package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class CheckBox {
	private final Style style;
	CheckBox(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().checkbox().textBackground(); }
	@JsonProperty Color foreground() { return style.theme().checkbox().textForeground(); }
	@JsonProperty Color select() { return null; } // no effect
	@JsonProperty Color disabledText() { return style.theme().checkbox().disabledBorder(); }
}
