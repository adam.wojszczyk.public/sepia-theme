package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.Effect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

@Effect("ToolWindow Run")
class DefaultTabs {
	private final Style style;
	DefaultTabs(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base().darker(2); }
	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Color hoverBackground() { return style.theme().tab().hover(); }
	@JsonProperty Color inactiveUnderlineColor() { return style.theme().tab().underline(); }
	@JsonProperty Color underlineColor() { return style.theme().tab().underline(); }
	@JsonProperty Color underlinedTabBackground() { return style.theme().tab().selected(); }
	@JsonProperty Color underlinedTabForeground() { return style.theme().foreground().base(); }
	@JsonProperty Integer underlineHeight() { return style.theme().tab().settingsUnderlineHeight(); }
}
