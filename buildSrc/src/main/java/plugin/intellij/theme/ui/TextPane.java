package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class TextPane {
	private final Style style;
	TextPane(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.scheme().background().base(); }
	@JsonProperty Color caretForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color inactiveBackground() { return style.theme().background().readOnly(); }
	@JsonProperty Color inactiveForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionBackground() { return style.scheme().background().selectedLine(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
}
