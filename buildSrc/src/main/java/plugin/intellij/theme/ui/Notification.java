package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Notification { // Balloon - bottom right side notifications
	private final Style style;
	Notification(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Color errorBackground() { return background(); }
	@JsonProperty Color errorBorderColor() { return style.theme().borderColor(); }
	@JsonProperty Color errorForeground() { return style.theme().foreground().base(); }
	@JsonProperty MoreButton MoreButton() { return new MoreButton(style, this); }
	@JsonProperty ToolWindow ToolWindow() { return new ToolWindow(style); }
	@JsonProperty WelcomeScreen WelcomeScreen() { return new WelcomeScreen(style); }

	static class MoreButton {
		private final Style style;
		private final Notification notification;
		MoreButton(Style style, Notification notification) {
			this.style = requireNonNull(style);
			this.notification = requireNonNull(notification);
		}

		@JsonProperty Color foreground() { return style.theme().foreground().disabled(); }
		@JsonProperty Color background() { return this.notification.background().darker(); }
		@JsonProperty Color innerBorderColor() { return this.style.theme().background().base().darker().darker(); }
	}

	static class ToolWindow { // over tools colorful notifications
		private final Style style;
		ToolWindow(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color informativeBackground() { return style.success().brighter(3); }
		@JsonProperty Color informativeBorderColor() { return informativeBackground(); }
		@JsonProperty Color informativeForeground() { return style.theme().foreground().base(); }
		@JsonProperty Color warningBackground() { return style.theme().warning().brighter(); }
		@JsonProperty Color warningBorderColor() { return warningBackground(); }
		@JsonProperty Color warningForeground() { return style.theme().foreground().base(); }
		@JsonProperty Color errorBackground() { return style.error().brighter(3); }
		@JsonProperty Color errorBorderColor() { return errorBackground(); }
		@JsonProperty Color errorForeground() { return style.theme().foreground().base(); }
	}

	static class WelcomeScreen {
		private final Style style;
		WelcomeScreen(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color separatorColor() { return style.theme().borderColor(); }
	}
}
