package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.Effect;
import plugin.intellij.UnknownEffect;
import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class RunWidget {
	private final Palette palette;
	private final Style style;
	private final MainToolbar mainToolbar;
	RunWidget(Palette palette, Style style, MainToolbar mainToolbar) {
		this.palette = requireNonNull(palette);
		this.style = requireNonNull(style);
		this.mainToolbar = requireNonNull(mainToolbar);
	}

	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@Effect("More actions vertical dots filling") @JsonProperty Color iconColor() { return hoverBackground().darker(2); }
	@JsonProperty Color runningBackground() { return runIconColor(); }
	@JsonProperty Color runningIconColor() { return hoverBackground(); }
	@UnknownEffect @JsonProperty Color hoverBackground() { return mainToolbar.background().darker(); }
	@UnknownEffect @JsonProperty Color pressedBackground() { return hoverBackground(); }
	@JsonProperty Color runIconColor() { return palette.aqua(); }
	@JsonProperty Color stopBackground() { return palette.red(); }
}
