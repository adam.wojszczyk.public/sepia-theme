package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.UnknownEffect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Button {
	private final Style style;
	Button(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Integer ark() { return null; }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color background() { return style.theme().button().around(); }
	@JsonProperty Color disabledText() { return style.theme().foreground().disabled(); }
	@JsonProperty Color shadowColor() { return null; }
	@JsonProperty Integer shadowWidth() { return 0; }
	@JsonProperty Color startBackground() { return style.theme().button().ordinary().background(); }
	@JsonProperty Color endBackground() { return style.theme().button().ordinary().background(); }
	@JsonProperty Color startBorderColor() { return style.theme().button().ordinary().border(); }
	@JsonProperty Color endBorderColor() { return style.theme().button().ordinary().border(); }
	@JsonProperty Color disabledBorderColor() { return style.theme().button().ordinary().disabledBorder(); }
	@JsonProperty Color focusedBorderColor() { return style.theme().button().ordinary().focusedBorder(); }
	@JsonProperty("default") Default defaultValue() { return new Default(style); }

	static class Default {
		private final Style style;
		Default(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color foreground() { return style.theme().foreground().base(); }
		@JsonProperty Color startBackground() { return style.theme().button().Default().background(); }
		@JsonProperty Color endBackground() { return style.theme().button().Default().background(); }
		@JsonProperty Color startBorderColor() { return style.theme().button().Default().border(); }
		@JsonProperty Color endBorderColor() { return style.theme().button().Default().border(); }
		@JsonProperty Color focusedBorderColor() { return style.theme().button().Default().focusedBorder(); }
		@JsonProperty @UnknownEffect Color focusColor() { return null; }
	}
}
