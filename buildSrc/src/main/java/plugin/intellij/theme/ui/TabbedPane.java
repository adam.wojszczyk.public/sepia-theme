package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.Effect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

@Effect("Settings -> editor -> code style")
class TabbedPane {
	private final Style style;
	TabbedPane(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().tab().inBackground(); }
	@JsonProperty Color contentAreaColor() { return style.theme().background().base(); }
	@JsonProperty Color disabledForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color focus() { return style.theme().tab().selected(); }
	@JsonProperty Color focusColor() { return style.theme().tab().selected(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color hoverColor() { return style.theme().tab().hover(); }
	@JsonProperty int tabSelectionHeight() { return style.theme().tab().settingsUnderlineHeight(); }
	@JsonProperty Color underlineColor() { return style.theme().tab().underline(); }
	@JsonProperty Color disabledUnderlineColor() { return underlineColor(); }
}
