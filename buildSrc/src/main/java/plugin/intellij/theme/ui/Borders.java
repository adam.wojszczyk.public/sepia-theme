package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Borders {
	private final Style style;
	Borders(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color color() { return style.theme().borderColor(); }
	@JsonProperty Color ContrastBorderColor() { return style.theme().borderColor(); }
}
