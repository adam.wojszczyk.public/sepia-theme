package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.Effect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class List {
	private final Style style;
	List(Style style) { this.style = requireNonNull(style); }

	@JsonProperty @Effect("used also for branch search field") Color background() { return style.theme().background().input(); }
	@JsonProperty Color dropLineColor() { return style.theme().borderColor(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color hoverBackground() { return style.theme().background().hover(); }
	@JsonProperty Color hoverInactiveBackground() { return style.theme().background().hover(); }
	@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionInactiveBackground() { return style.theme().background().selectedInactive(); }
	@JsonProperty Color selectionInactiveForeground() { return style.theme().foreground().base(); }
}
