package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class TextField {
	private final Style style;
	TextField(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.scheme().background().base(); }
	@JsonProperty Color caretForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color darkShadow() { return null; }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color highlight() { return null; } // unknown effect
	@JsonProperty Color inactiveBackground() { return style.theme().background().base(); } // Change signature name
	@JsonProperty Color inactiveForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionBackground() { return style.scheme().background().selectedText(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
}
