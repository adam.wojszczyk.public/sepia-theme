package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class FormattedTextField {
	private final Style style;
	FormattedTextField(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base().brighter(); }
	@JsonProperty Color caretForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color inactiveBackground() { return background(); }
	@JsonProperty Color inactiveForeground() { return style.theme().foreground().disabled(); }
}
