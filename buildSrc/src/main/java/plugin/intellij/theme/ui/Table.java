package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Table {
	private final Style style;
	Table(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color alternativeRowBackground() { return style.theme().table().alternativeRowBackground(); }
	@JsonProperty Color background() { return style.theme().table().background(); }
	@JsonProperty Color dropLineColor() { return null; } // unknown effect
	@JsonProperty Color dropLineShortColor() { return null; } // unknown effect
	@JsonProperty Color focusCellBackground() { return style.theme().table().focusBackground(); }
	@JsonProperty Color focusCellForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color gridColor() { return style.theme().table().grid(); }
	@JsonProperty Color hoverBackground() { return style.theme().table().hoverBackground(); }
	@JsonProperty Color hoverInactiveBackground() { return hoverBackground(); }
	@JsonProperty Color lightSelectionBackground() { return style.theme().table().selectionBackground(); }
	@JsonProperty Color lightSelectionForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color lightSelectionInactiveBackground() { return style.theme().table().selectionInactiveBackground(); }
	@JsonProperty Color lightSelectionInactiveForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionBackground() { return style.theme().table().selectionBackground(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionInactiveBackground() { return style.theme().table().selectionInactiveBackground(); }
	@JsonProperty Color selectionInactiveForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color sortIconColor() { return null; } // unknown effect
	@JsonProperty Color stripeColor() { return style.theme().table().stripe(); }
}
