package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class SearchEverywhere {
	private final Style style;
	private final Popup popup;
	SearchEverywhere(Style style, Popup popup) {
		this.style = requireNonNull(style);
		this.popup = requireNonNull(popup);
	}

	@JsonProperty Advertiser Advertiser() { return new Advertiser(style, popup); }
	@JsonProperty Header Header() { return new Header(style); }
	@JsonProperty List List() { return new List(style); }
	@JsonProperty SearchField SearchField() { return new SearchField(style); }
	@JsonProperty Tab Tab() { return new Tab(style); }

	static class Advertiser {
		private final Style style;
		private final Popup popup;
		Advertiser(Style style, Popup popup) {
			this.style = requireNonNull(style);
			this.popup = requireNonNull(popup);
		}

		@JsonProperty Color background() { return popup.background().darker(); }
		@JsonProperty String borderInsets() { return null; }
		@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	}

	static class Header {
		private final Style style;
		Header(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.theme().background().base().darker(); }
	}

	static class List {
		private final Style style;
		List(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color separatorColor() { return style.theme().borderColor(); }
		@JsonProperty Color separatorForeground() { return style.theme().foreground().base(); }
	}

	static class SearchField {
		private final Style style;
		SearchField(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.scheme().background().base(); }
		@JsonProperty Color borderColor() { return style.theme().borderColor(); }
		@JsonProperty Color infoForeground() { return style.theme().foreground().info(); }
	}

	static class Tab {
		private final Style style;
		Tab(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color selectedBackground() { return style.theme().background().selected(); }
		@JsonProperty Color selectedForeground() { return style.theme().foreground().base(); }
	}
}
