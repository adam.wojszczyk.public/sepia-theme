package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class OptionPane {
	private final Style style;
	OptionPane(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color messageForeground() { return style.theme().foreground().base(); }
}
