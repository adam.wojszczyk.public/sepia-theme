package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class ParameterInfo {
	private final Style style;
	ParameterInfo(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Color currentOverloadBackground() { return style.theme().background().selected(); }
	@JsonProperty Color currentParameterForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color disabledForeground() { return style.theme().foreground().disabled(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color infoForeground() { return style.theme().foreground().info(); }
	@JsonProperty Color lineSeparatorColor() { return style.theme().borderColor(); }
}
