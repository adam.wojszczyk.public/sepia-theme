package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class TitlePane { // IDE title
	private final Style style;
	TitlePane(Style style) { this.style = requireNonNull(style); }
	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color inactiveBackground() { return style.theme().background().readOnly(); }
	@JsonProperty Color inactiveInfoForeground() { return style.theme().foreground().info(); }
	@JsonProperty Color infoForeground() { return style.theme().foreground().info(); }

	@JsonProperty Button Button() { return new Button(style); }

	static class Button {
		private final Style style;
		Button(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color hoverBackground() { return style.theme().background().hover(); }
	}
}
