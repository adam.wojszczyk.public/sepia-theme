package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Plugins {
	private final Palette palette;
	private final Style style;
	Plugins(Palette palette, Style style) {
		this.palette = requireNonNull(palette);
		this.style = requireNonNull(style);
	}

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color disabledForeground() { return style.theme().foreground().disabled(); }
	@JsonProperty Color hoverBackground() { return style.theme().background().hover(); }
	@JsonProperty Color lightSelectionBackground() { return style.theme().background().selected(); }
	@JsonProperty Color tagForeground() { return background(); }
	@JsonProperty Color tagBackground() { return palette.blue(); }
	@JsonProperty Color eapTagBackground() { return palette.red(); }
	@JsonProperty Button Button() { return new Button(palette, this); }
	@JsonProperty SearchField SearchField() { return new SearchField(style, this); }
	@JsonProperty SectionHeader SectionHeader() { return new SectionHeader(style, this); }
	@JsonProperty Tab Tab() { return new Tab(); }

	static class Button {
		private final Palette palette;
		private final Plugins plugins;
		Button(Palette palette, Plugins plugins) {
			this.palette = requireNonNull(palette);
			this.plugins = requireNonNull(plugins);
		}

		@JsonProperty Color installForeground() { return plugins.background(); }
		@JsonProperty Color installBackground() { return palette.aqua(); }
		@JsonProperty Color installFillBackground() { return installBackground(); }
		@JsonProperty Color installFocusedBackground() { return palette.blue(); }
		@JsonProperty Color installBorderColor() { return installBackground(); }
		@JsonProperty Color updateForeground() { return plugins.background(); }
		@JsonProperty Color updateBackground() { return installBackground(); }
		@JsonProperty Color updateBorderColor() { return installBorderColor(); }
	}

	static class SearchField {
		private final Style style;
		private final Plugins plugins;
		SearchField(Style style, Plugins plugins) {
			this.style = requireNonNull(style);
			this.plugins = requireNonNull(plugins);
		}

		@JsonProperty Color background() { return this.plugins.background().brighter(); }
		@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	}

	static class SectionHeader {
		private final Style style;
		private final Plugins plugins;
		SectionHeader(Style style, Plugins plugins) {
			this.style = requireNonNull(style);
			this.plugins = requireNonNull(plugins);
		}

		@JsonProperty Color background() { return this.plugins.background().darker(); }
		@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	}

	static class Tab {
		@JsonProperty Color selectedForeground() { return null; } // unknown effect
		@JsonProperty Color hoverBackground() { return null; } // unknown effect
		@JsonProperty Color selectedBackground() { return null; } // unknown effect
	}
}
