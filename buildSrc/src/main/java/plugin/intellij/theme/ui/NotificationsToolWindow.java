package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class NotificationsToolWindow {
	private final Style style;
	NotificationsToolWindow(Style style) { this.style = requireNonNull(style); }

	private Color background() { return style.theme().background().base().darker(); }
	private Color hoverBackground() { return background().darker(); }

	@JsonProperty NewNotification newNotification() { return new NewNotification(this); }
	@JsonProperty Notification Notification() { return new Notification(this); }

	static class NewNotification {
		private final NotificationsToolWindow notificationsToolWindow;
		NewNotification(NotificationsToolWindow notificationsToolWindow) { this.notificationsToolWindow = requireNonNull(notificationsToolWindow); }

		@JsonProperty Color background() { return this.notificationsToolWindow.background(); }
		@JsonProperty Color hoverBackground() { return this.notificationsToolWindow.hoverBackground(); }
	}

	static class Notification {
		private final NotificationsToolWindow notificationsToolWindow;
		Notification(NotificationsToolWindow notificationsToolWindow) { this.notificationsToolWindow = requireNonNull(notificationsToolWindow); }

		@JsonProperty Color hoverBackground() { return this.notificationsToolWindow.hoverBackground(); }
	}
}
