package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class MainMenu {
	private final Style style;
	MainMenu(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
	@JsonProperty Color selectionForeground() { return foreground(); }
}
