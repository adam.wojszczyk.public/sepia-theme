package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class EditorTabs {
	private final Style style;
	EditorTabs(Style style) {
		this.style = requireNonNull(style);
	}

	@JsonProperty Color underlinedTabForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color underlinedTabBackground() { return style.scheme().background().base(); }
	@JsonProperty Color hoverBackground() { return underlinedTabBackground(); }
	@JsonProperty Color background() { return style.scheme().background().base().darker(3); }
	@JsonProperty Color borderColor() { return underlinedTabBackground(); }
	@JsonProperty int underlineHeight() { return style.theme().tab().editorUnderlineHeight(); }
	@JsonProperty Color underlineColor() { return style.theme().tab().underline(); }
	@JsonProperty Color inactiveUnderlineColor() { return underlineColor(); }
	@JsonProperty boolean tabInsets() { return false; }  // no effect
	@JsonProperty Color inactiveColoredFileBackground() { return null; } // masks
}
