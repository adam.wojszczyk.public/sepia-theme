package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.UnknownEffect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Slider {
	private final Style style;
	Slider(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color trackColor() { return style.theme().background().base().darker(2); }
	@JsonProperty Color tickColor() { return trackColor().darker(); }
	@JsonProperty Color buttonColor() { return trackColor().darker(); }
	@JsonProperty Color buttonBorderColor() { return buttonColor().darker(); }
	@JsonProperty @UnknownEffect Color focus() { return null; }
	@JsonProperty @UnknownEffect Color highlight() { return null; }
	@JsonProperty @UnknownEffect Color shadow() { return null; }
}
