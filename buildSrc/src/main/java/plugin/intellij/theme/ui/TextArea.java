package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class TextArea {
	private final Style style;
	TextArea(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.scheme().background().base(); }
	@JsonProperty Color caretForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color inactiveBackground() { return style.scheme().background().base(); }
	@JsonProperty Color inactiveForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionBackground() { return style.scheme().background().selectedText(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
}
