package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class MenuItem {
	private final Style style;
	public MenuItem(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color acceleratorForeground() { return style.theme().foreground().disabled(); }
	@JsonProperty Color acceleratorSelectionForeground() { return acceleratorForeground().darker(1); }
	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color disabledBackground() { return style.theme().background().base(); }
	@JsonProperty Color disabledForeground() { return style.theme().foreground().disabled(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
}
