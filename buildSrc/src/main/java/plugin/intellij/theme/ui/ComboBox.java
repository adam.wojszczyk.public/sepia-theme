package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class ComboBox {
	private final Style style;
	ComboBox(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color disabledForeground() { return style.theme().foreground().disabled(); }
	@JsonProperty Color modifiedItemForeground() { return style.theme().foreground().modified(); }
	@JsonProperty Color background() { return style.theme().background().base().brighter(); }
	@JsonProperty Color disabledBackground() { return style.theme().background().base(); } // deprecated but it works, no other ways of setting this value in code
	@JsonProperty Color nonEditableBackground() { return background(); }
	@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
	@JsonProperty ArrowButton ArrowButton() { return new ArrowButton(style, background()); }

	static class ArrowButton {
		private final Style style;
		private final Color background;
		public ArrowButton(Style style, Color background) {
			this.style = requireNonNull(style);
			this.background = requireNonNull(background);
		}

		@JsonProperty Color iconColor() { return style.theme().foreground().base(); }
		@JsonProperty Color disabledIconColor() { return style.theme().foreground().disabled(); }
		@JsonProperty Color background() { return background; }
		@JsonProperty Color nonEditableBackground() { return background(); }
	}
}
