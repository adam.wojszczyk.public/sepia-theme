package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class TableHeader {
	private final Style style;
	TableHeader(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().table().header().background(); }
	@JsonProperty Color bottomSeparatorColor() { return style.theme().table().header().separator(); }
	@JsonProperty Color cellBorder() { return Color.empty(); }
	@JsonProperty Color focusCellBackground() { return style.theme().table().header().focusCellBackground(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color separatorColor() { return style.theme().table().header().separator(); }
}
