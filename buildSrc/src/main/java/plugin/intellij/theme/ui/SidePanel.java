package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.Effect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

@Effect("Settings->Left Panel")
@Effect("Project Structure->Left Panel")
class SidePanel {
	private final Style style;
	SidePanel(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base().darker(); }
}
