package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.Effect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

@Effect("soft wrap preview floating icon")
class ToolBar {
	private final Style style;
	ToolBar(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color borderHandleColor() { return null; } // unknown effect
	@JsonProperty Color darkShadow() { return null; }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color highlight() { return null; } // unknown effect
	@JsonProperty Color light() { return null; }
	@JsonProperty Color shadow() { return null; }
	@JsonProperty Color floatingForeground() { return style.theme().foreground().base(); }
}
