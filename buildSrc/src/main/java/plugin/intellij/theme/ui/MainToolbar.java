package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.Effect;
import plugin.intellij.UnknownEffect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class MainToolbar {
	private final Style style;
	MainToolbar(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base().darker(2); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color inactiveBackground() { return background().brighter(); }

	@JsonProperty Dropdown Dropdown() { return new Dropdown(style, this); }
	@JsonProperty Icon Icon() { return new Icon(this); }

	static class Dropdown {
		private final Style style;
		private final MainToolbar mainToolbar;
		Dropdown(Style style, MainToolbar mainToolbar) {
			this.style = requireNonNull(style);
			this.mainToolbar = requireNonNull(mainToolbar);
		}

		@JsonProperty Color background() { return mainToolbar.background(); }
		@JsonProperty Color foreground() { return style.theme().foreground().base(); }
		@JsonProperty Color hoverBackground() { return background().darker(); }
		@JsonProperty @UnknownEffect Color transparentHoverBackground() { return Color.empty(); }
		@UnknownEffect("Zero is neutral value") @JsonProperty int maxWidth() { return 0; }
	}

	@Effect("Main Menu, Run, Debug, More Actions, Run Configurations")
	static class Icon {
		private final MainToolbar mainToolbar;
		Icon(MainToolbar mainToolbar) {
			this.mainToolbar = requireNonNull(mainToolbar);
		}

		@JsonProperty Color background() { return mainToolbar.background(); }
		@JsonProperty Color hoverBackground() { return background().darker(); }
		@JsonProperty Color pressedBackground() { return hoverBackground().darker(); }
	}
}
