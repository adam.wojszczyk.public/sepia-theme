package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Component {
	private final Style style;
	Component(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color focusedBorderColor() { return style.theme().borderColor(); }
	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Integer focusWidth() { return null; }
	@JsonProperty Integer arc() { return null; }
	@JsonProperty Color disabledBorderColor() { return style.theme().borderColor().brighter(); }
	@JsonProperty Color errorFocusColor() { return style.error(); }
	@JsonProperty Color focusColor() { return style.scheme().background().selectedText(); }
	@JsonProperty Color hoverIconColor() { return null; }
	@JsonProperty Color iconColor() { return null; }
	@JsonProperty Color inactiveErrorFocusColor() { return style.error().opacity(0.5); }
	@JsonProperty Color inactiveWarningFocusColor() { return style.theme().warning().opacity(0.5); }
	@JsonProperty Color infoForeground() { return style.theme().foreground().info(); }
	@JsonProperty Color warningFocusColor() { return style.theme().warning(); }
}
