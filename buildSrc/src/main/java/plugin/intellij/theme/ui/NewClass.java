package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class NewClass {
	private final Style style;
	public NewClass(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Integer separatorWidth() { return null; }
	@JsonProperty Panel Panel() { return new Panel(style); }
	@JsonProperty SearchField SearchField() { return new SearchField(style); }

	static class Panel {
		private final Style style;
		Panel(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.theme().background().base(); }
	}

	static class SearchField {
		private final Style style;
		SearchField(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.theme().background().input(); }
	}
}
