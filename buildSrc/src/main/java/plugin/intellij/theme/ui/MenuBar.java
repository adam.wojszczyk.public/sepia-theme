package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class MenuBar {
	private final Style style;
	MenuBar(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Color disabledBackground() { return style.theme().background().base(); }
	@JsonProperty Color disabledForeground() { return style.theme().foreground().disabled(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color highlight() { return null; }
	@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
	@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color shadow() { return null; }
}
