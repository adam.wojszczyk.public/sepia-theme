package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.intellij.Effect;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

@Effect("Recent files")
@Effect("Find in files")
class Popup {
	private final Style style;
	private final ComplexPopup complexPopup;
	Popup(Style style, ComplexPopup complexPopup) {
		this.style = requireNonNull(style);
		this.complexPopup = requireNonNull(complexPopup);
	}

	@JsonProperty Color background() { return style.scheme().background().base(); }
	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Color inactiveBorderColor() { return style.theme().borderColor(); }
	@JsonProperty Color innerBorderColor() { return style.theme().borderColor(); }
	@JsonProperty boolean paintBorder() { return false; } // on Mac
	@JsonProperty Color separatorColor() { return style.theme().borderColor(); }
	@JsonProperty Color separatorForeground() { return style.theme().foreground().base(); }
	@JsonProperty Header Header() { return new Header(complexPopup); }
	@JsonProperty Advertiser Advertiser() { return new Advertiser(style, this); }
	@JsonProperty Toolbar Toolbar() { return new Toolbar(style); }

	static class Header {
		private final ComplexPopup complexPopup;
		Header(ComplexPopup complexPopup) {
			this.complexPopup = requireNonNull(complexPopup);
		}

		@JsonProperty Color activeBackground() { return complexPopup.Header().background(); }
		@JsonProperty Color inactiveBackground() { return activeBackground().brighter(); }
	}

	static class Advertiser {
		private final Style style;
		private final Popup popup;
		Advertiser(Style style, Popup popup) {
			this.style = requireNonNull(style);
			this.popup = requireNonNull(popup);
		}

		@JsonProperty Color background() { return popup.background().darker(); }
		@JsonProperty Color borderColor() { return style.theme().borderColor(); }
		@JsonProperty Integer borderInsets() { return null; }
		@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	}

	static class Toolbar {
		private final Style style;
		Toolbar(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.theme().background().base(); }
		@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	}
}
