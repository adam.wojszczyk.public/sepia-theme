package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;

class PopupMenuSeparator {
	@JsonProperty Integer height() { return null; }
	@JsonProperty Integer stripeIndent() { return null; }
	@JsonProperty Integer stripeWidth() { return null; }
}
