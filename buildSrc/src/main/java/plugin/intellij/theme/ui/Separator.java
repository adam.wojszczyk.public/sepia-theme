package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Separator {
	private final Style style;
	Separator(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color separatorColor() { return style.theme().borderColor(); }
}
