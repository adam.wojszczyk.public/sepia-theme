package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class StatusBar {
	private final Style style;
	StatusBar(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color LightEditBackground() { return background(); }
	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Breadcrumbs Breadcrumbs() { return new Breadcrumbs(style); }

	static class Breadcrumbs {
		private final Style style;
		Breadcrumbs(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color foreground() { return style.theme().foreground().base(); }
		@JsonProperty Color hoverForeground() { return style.theme().foreground().base(); }
		@JsonProperty Color hoverBackground() { return style.theme().background().hover(); }
		@JsonProperty Color selectionForeground() { return style.theme().foreground().base(); }
		@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
		@JsonProperty Color selectionInactiveForeground() { return style.theme().foreground().base(); }
		@JsonProperty Color selectionInactiveBackground() { return style.theme().background().selectedInactive(); }
		@JsonProperty Color floatingBackground() { return style.theme().background().base(); }
		@JsonProperty Color floatingForeground() { return style.theme().foreground().base(); }
	}
}
