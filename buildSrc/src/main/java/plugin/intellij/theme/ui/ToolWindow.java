package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class ToolWindow {
	private final Palette palette;
	private final Style style;
	ToolWindow(Palette palette, Style style) {
		this.palette = requireNonNull(palette);
		this.style = requireNonNull(style);
	}

	@JsonProperty Color background() { return style.theme().background().base(); }

	@JsonProperty Button Button() { return new Button(style); }
	@JsonProperty Header Header() { return new Header(style); }
	@JsonProperty HeaderCloseButton HeaderCloseButton() { return new HeaderCloseButton(palette); }
	@JsonProperty HeaderTab HeaderTab() { return new HeaderTab(style); }

	static class Button {
		private final Style style;
		Button(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color hoverBackground() { return style.theme().background().hover(); }
		@JsonProperty Color selectedBackground() { return style.theme().background().selected(); }
		@JsonProperty Color selectedForeground() { return style.theme().foreground().base(); }
	}

	static class Header {
		private final Style style;
		Header(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color inactiveBackground() { return style.theme().tab().inBackgroundInactive(); }
		@JsonProperty Color borderColor() { return style.theme().tab().borderColor(); }
		@JsonProperty Color background() { return style.theme().tab().inBackground(); }
	}

	static class HeaderCloseButton {
		private final Palette palette;
		HeaderCloseButton(Palette palette) { this.palette = requireNonNull(palette); }

		@JsonProperty Color background() { return palette.red(); }
	}

	static class HeaderTab {
		private final Style style;
		HeaderTab(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color hoverBackground() { return style.theme().tab().hover(); }
		@JsonProperty Color hoverInactiveBackground() { return hoverBackground(); }
		@JsonProperty Color selectedInactiveBackground() { return style.theme().tab().selected(); }
		@JsonProperty Color underlinedTabBackground() { return style.theme().tab().selected(); }
		@JsonProperty Color underlinedTabInactiveBackground() { return style.theme().tab().selected(); }
		@JsonProperty int underlineHeight() { return 0; }
		@JsonProperty Color underlineColor() { return null; }
		@JsonProperty Color inactiveUnderlineColor() { return null; }
	}
}
