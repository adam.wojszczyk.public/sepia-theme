package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class FileColor {
	private final Palette palette;
	private final Style style;
	FileColor(Palette palette, Style style) {
		this.palette = requireNonNull(palette);
		this.style = requireNonNull(style);
	}

	@JsonProperty
	Color Yellow() {
		return style.theme().background().readOnly();
	}

	@JsonProperty
	Color Green() {
		return palette.green().brighter(2).opacity(0.13);
	}

	@JsonProperty
	Color Blue() {
		return palette.blue().brighter();
	}

	@JsonProperty
	Color Violet() {
		return palette.purple().brighter();
	}

	@JsonProperty
	Color Orange() {
		return palette.orange().brighter();
	}

	@JsonProperty
	Color Rose() {
		return palette.red().brighter();
	}
}
