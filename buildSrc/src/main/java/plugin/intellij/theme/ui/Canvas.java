package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Canvas {
	private final Style style;
	Canvas(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Tooltip Tooltip() { return new Tooltip(style); }

	static class Tooltip {
		private final Style style;
		Tooltip(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.theme().background().base(); }
	}
}
