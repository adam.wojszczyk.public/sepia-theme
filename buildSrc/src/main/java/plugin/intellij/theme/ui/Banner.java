package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

public class Banner {
	private final Style style;
	Banner(Style style) { this.style = requireNonNull(style); }

	// Notification about decompiled .class in an editor tab
	@JsonProperty Color infoBackground() { return style.scheme().background().readOnly().darker(); }
	@JsonProperty Color infoBorderColor() { return infoBackground().darker(2); }
	@JsonProperty Color successBackground() { return style.success(); }
	@JsonProperty Color successBorderColor() { return successBackground().darker(2); }
	@JsonProperty Color warningBackground() { return style.theme().warning(); }
	@JsonProperty Color warningBorderColor() { return warningBackground().darker(2); }
	@JsonProperty Color errorBackground() { return style.error(); }
	@JsonProperty Color errorBorderColor() { return errorBackground().darker(2); }
}
