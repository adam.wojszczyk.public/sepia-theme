package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Label {
	private final Style style;
	Label(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); } // unknown effect
	@JsonProperty Color disabledText() { return style.theme().foreground().disabled(); } // unknown effect
	@JsonProperty Color disabledForeground() { return style.theme().foreground().disabled(); } // unknown effect
	@JsonProperty Color errorForeground() { return style.error(); } // unknown effect
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color infoForeground() { return style.theme().foreground().info(); }
	@JsonProperty Color selectedForeground() { return style.theme().foreground().base(); } // unknown effect
}
