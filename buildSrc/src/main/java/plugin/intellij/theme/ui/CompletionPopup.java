package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

// has documentation background
class CompletionPopup {
	private final Palette palette;
	private final Style style;
	CompletionPopup(Palette palette, Style style) {
		this.palette = requireNonNull(palette);
		this.style = requireNonNull(style);
	}

	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color matchForeground() { return palette.blue().darker(); }
	@JsonProperty Color nonFocusedMask() { return Color.empty(); }
	@JsonProperty Color selectionInactiveBackground() { return style.scheme().background().documentation().darker(2); }
	@JsonProperty Color selectionBackground() { return style.scheme().background().documentation().darker(2).darker(); }

	@JsonProperty Advertiser Advertiser() { return new Advertiser(style); }

	static class Advertiser {
		private final Style style;
		Advertiser(Style style) {
			this.style = requireNonNull(style);
		}

		@JsonProperty Color background() { return style.theme().background().base().darker(); }
		@JsonProperty Color borderColor() { return Color.empty(); }
		@JsonProperty String borderInsets() { return null; } // no effect
		@JsonProperty Color foreground() { return style.theme().foreground().info(); }
	}
}
