package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

public class UI {
	private final Palette palette;
	private final Style style;
	public UI(Style style, Palette palette) {
		this.palette = requireNonNull(palette);
		this.style = requireNonNull(style);
	}

	@JsonProperty ActionButton ActionButton() { return new ActionButton(style); }
	@JsonProperty Banner Banner() { return new Banner(style); }
	@JsonProperty BigSpinner BigSpinner() { return new BigSpinner(style); }
	@JsonProperty Borders Borders() { return new Borders(style); }
	@JsonProperty Button Button() { return new Button(style); }
	@JsonProperty Canvas Canvas() { return new Canvas(style); }
	@JsonProperty CheckBox CheckBox() { return new CheckBox(style); }
	@JsonProperty ComboBox ComboBox() { return new ComboBox(style); }
	@JsonProperty ComboBoxButton ComboBoxButton() { return new ComboBoxButton(style); }
	@JsonProperty CompletionPopup CompletionPopup() { return new CompletionPopup(palette, style); }
	@JsonProperty ComplexPopup ComplexPopup() { return new ComplexPopup(style); }
	@JsonProperty Component Component() { return new Component(style); }
	@JsonProperty DefaultTabs DefaultTabs() { return new DefaultTabs(style); }
	@JsonProperty Editor Editor() { return new Editor(style); }
	@JsonProperty EditorPane EditorPane() { return new EditorPane(style); }
	@JsonProperty EditorTabs EditorTabs() { return new EditorTabs(style); }
	@JsonProperty FileColor FileColor() { return new FileColor(palette, style); }
	@JsonProperty FormattedTextField FormattedTextField() { return new FormattedTextField(style); }
	@JsonProperty GotItTooltip GotItTooltip() { return new GotItTooltip(style); }
	@JsonProperty Group Group() { return new Group(style); }
	@JsonProperty Label Label() { return new Label(style); }
	@JsonProperty Link Link() { return new Link(style); }
	@JsonProperty List List() { return new List(style); }
	@JsonProperty MainMenu MainMenu() { return new MainMenu(style); }
	@JsonProperty MainToolbar MainToolbar() { return new MainToolbar(style); }
	@JsonProperty MemoryIndicator MemoryIndicator() { return new MemoryIndicator(style); }
	@JsonProperty Menu Menu() { return new Menu(style); }
	@JsonProperty MenuBar MenuBar() { return new MenuBar(style); }
	@JsonProperty MenuItem MenuItem() { return new MenuItem(style); }
	@JsonProperty NavBar NavBar() { return new NavBar(style); }
	@JsonProperty NewClass NewClass() { return new NewClass(style); }
	@JsonProperty Notification Notification() { return new Notification(style); }
	@JsonProperty NotificationsToolWindow NotificationsToolwindow() { return new NotificationsToolWindow(style); }
	@JsonProperty OptionPane OptionPane() { return new OptionPane(style); }
	@JsonProperty Panel Panel() { return new Panel(style); }
	@JsonProperty ParameterInfo ParameterInfo() { return new ParameterInfo(style); }
	@JsonProperty Plugins Plugins() { return new Plugins(palette, style); }
	@JsonProperty Popup Popup() { return new Popup(style, ComplexPopup()); }
	@JsonProperty PopupMenu PopupMenu() { return new PopupMenu(style); }
	@JsonProperty PopupMenuSeparator PopupMenuSeparator() { return new PopupMenuSeparator(); }
	@JsonProperty ProgressBar ProgressBar() { return new ProgressBar(style); }
	@JsonProperty RadioButton RadioButton() { return new RadioButton(style); }
	@JsonProperty RunWidget RunWidget() { return new RunWidget(palette, style, MainToolbar()); }
	@JsonProperty ScrollBar ScrollBar() { return new ScrollBar(style); }
	@JsonProperty SearchEverywhere SearchEverywhere() { return new SearchEverywhere(style, Popup()); }
	@JsonProperty SearchMatch SearchMatch() { return new SearchMatch(style); }
	@JsonProperty SegmentedButton SegmentedButton() { return new SegmentedButton(style); }
	@JsonProperty Separator Separator() { return new Separator(style); }
	@JsonProperty Settings Settings() { return new Settings(style); }
	@JsonProperty SidePanel SidePanel() { return new SidePanel(style); }
	@JsonProperty Slider Slider() { return new Slider(style); }
	@JsonProperty SpeedSearch SpeedSearch() { return new SpeedSearch(style); }
	@JsonProperty Spinner Spinner() { return new Spinner(style); }
	@JsonProperty StatusBar StatusBar() { return new StatusBar(style); }
	@JsonProperty TabbedPane TabbedPane() { return new TabbedPane(style); }
	@JsonProperty Table Table() { return new Table(style); }
	@JsonProperty TableHeader TableHeader() { return new TableHeader(style); }
	@JsonProperty TextArea TextArea() { return new TextArea(style); }
	@JsonProperty TextField TextField() { return new TextField(style); }
	@JsonProperty TextPane TextPane() { return new TextPane(style); }
	@JsonProperty TitlePane TitlePane() { return new TitlePane(style); }
	@JsonProperty ToolBar ToolBar() { return new ToolBar(style); }
	@JsonProperty("Toolbar.Floating.background") Color ToolBarFloatingBackground() { return style.theme().background().base().darker(); }
	@JsonProperty ToolTip ToolTip() { return new ToolTip(style); }
	@JsonProperty ToolWindow ToolWindow() { return new ToolWindow(palette, style); }
	@JsonProperty Tree Tree() { return new Tree(style); }
	@JsonProperty ValidationTooltip ValidationTooltip() { return new ValidationTooltip(style); }
	@JsonProperty VersionControl VersionControl() { return new VersionControl(palette, style); }
	@JsonProperty Viewport Viewport() { return new Viewport(style); }
	@JsonProperty WelcomeScreen WelcomeScreen() { return new WelcomeScreen(palette, style); }
}
