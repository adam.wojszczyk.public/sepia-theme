package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class ScrollBar {
	private final Style style;
	ScrollBar(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.scrollBar().trackColor().brighter().opaque(); }
	@JsonProperty Color track() { return null; }
	@JsonProperty Color trackColor() { return style.scrollBar().trackColor().brighter().opaque(); }
	@JsonProperty Color hoverTrackColor() { return style.scrollBar().hoverTrackColor().brighter().opaque(); }
	@JsonProperty Color trackHighlight() { return null; }
	@JsonProperty Color thumb() { return null; }
	@JsonProperty Color thumbColor() { return style.scrollBar().thumbColor().brighter().opaque(); }
	@JsonProperty Color thumbBorderColor() { return style.scrollBar().thumbBorderColor().brighter().opaque(); }
	@JsonProperty Color hoverThumbColor() { return thumbColor().darker(); }
	@JsonProperty Color hoverThumbBorderColor() { return thumbBorderColor().darker(); }
	@JsonProperty Color thumbShadow() { return null; }
	@JsonProperty Color thumbDarkShadow() { return null; }
	@JsonProperty Color thumbHighlight() { return null; }
	@JsonProperty Mac Mac() { return new Mac(this); }
	@JsonProperty Transparent Transparent() { return new Transparent(style); }

	static class Mac {
		private final ScrollBar scrollBar;
		Mac(ScrollBar scrollBar) { this.scrollBar = requireNonNull(scrollBar); }

		@JsonProperty Color hoverThumbBorderColor() { return scrollBar.hoverThumbBorderColor(); }
		@JsonProperty Color hoverThumbColor() { return scrollBar.hoverThumbColor(); }
		@JsonProperty Color hoverTrackColor() { return scrollBar.hoverTrackColor(); }
		@JsonProperty Color thumbBorderColor() { return scrollBar.thumbBorderColor(); }
		@JsonProperty Color thumbColor() { return scrollBar.thumbColor(); }
		@JsonProperty Transparent Transparent() { return scrollBar.Transparent(); }
	}

	static class Transparent {
		private final Style style;
		Transparent(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color trackColor() { return style.scrollBar().trackColor(); }
		@JsonProperty Color thumbColor() { return style.scrollBar().thumbColor(); }
		@JsonProperty Color thumbBorderColor() { return style.scrollBar().thumbBorderColor(); }
		@JsonProperty Color hoverTrackColor() { return style.scrollBar().thumbColor().opacity(0); }
		@JsonProperty Color hoverThumbColor() { return style.scrollBar().hoverThumbColor(); }
		@JsonProperty Color hoverThumbBorderColor() { return style.scrollBar().hoverThumbBorderColor(); }
	}
}
