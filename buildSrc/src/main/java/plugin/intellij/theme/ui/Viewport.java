package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Viewport {
	private final Style style;
	Viewport(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
}
