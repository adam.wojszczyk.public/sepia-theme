package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class WelcomeScreen {
	private final Palette palette;
	private final Style style;
	WelcomeScreen(Palette palette, Style style) {
		this.palette = requireNonNull(palette);
		this.style = requireNonNull(style);
	}

	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color captionBackground() { return palette.red(); }
	@JsonProperty Color captionForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color footerBackground() { return null; }
	@JsonProperty Color footerForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color groupIconBorderColor() { return style.theme().borderColor(); }
	@JsonProperty Color headerBackground() { return null; }
	@JsonProperty Color headerForeground() { return style.theme().foreground().base(); }
	@JsonProperty Color separatorColor() { return style.theme().borderColor(); }
	@JsonProperty Details Details() { return new Details(style); }
	@JsonProperty Projects Projects() { return new Projects(style); }
	@JsonProperty SidePanel SidePanel() { return new SidePanel(style); }

	static class Details {
		private final Style style;
		Details(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.theme().background().base(); }
	}

	static class Projects {
		private final Style style;
		Projects(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.theme().background().base(); }
		@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
		@JsonProperty Color selectionBorderColor() { return style.theme().borderColor(); }
		@JsonProperty Color selectionInactiveBackground() { return style.theme().background().selectedInactive(); }
		@JsonProperty Projects.Actions actions() { return new Projects.Actions(style); }

		static class Actions {
			private final Style style;
			Actions(Style style) { this.style = requireNonNull(style); }

			@JsonProperty Color background() { return style.theme().background().base(); }
			@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
			@JsonProperty Color selectionBorderColor() { return style.theme().borderColor(); }
		}
	}

	static class SidePanel {
		private final Style style;
		SidePanel(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.theme().background().base().darker(); }
	}
}
