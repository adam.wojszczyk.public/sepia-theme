package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class ActionButton {
	private final Style style;
	ActionButton(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color hoverBackground() { return style.theme().background().base().darker(3); }
	@JsonProperty Color hoverBorderColor() { return hoverBackground().darker(); }
	@JsonProperty Color pressedBackground() { return style.theme().background().selected(); }
	@JsonProperty Color pressedBorderColor() { return pressedBackground().darker(); }
}
