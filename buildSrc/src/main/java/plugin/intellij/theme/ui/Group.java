package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Group {
	private final Style style;
	Group(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color disabledSeparatorColor() { return style.theme().borderColor(); }
	@JsonProperty Color separatorColor() { return style.theme().borderColor(); }
}
