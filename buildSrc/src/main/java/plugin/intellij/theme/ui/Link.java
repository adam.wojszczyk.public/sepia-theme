package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Link {
	private final Style style;
	Link(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color activeForeground() { return foreground(); }
	@JsonProperty Color hoverForeground() { return foreground(); }
	@JsonProperty Color pressedForeground() { return foreground(); }
	@JsonProperty Color visitedForeground() { return foreground(); }
	private Color foreground() { return style.link(); }
}
