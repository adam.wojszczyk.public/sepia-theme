package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class GotItTooltip {
	private final Style style;
	GotItTooltip(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Color endBackground() { return null; }
	@JsonProperty Color endBorderColor() { return null; }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color linkForeground() { return style.link(); }
	@JsonProperty Color shortcutForeground() { return style.link(); }
	@JsonProperty Color startBackground() { return null; }
	@JsonProperty Color startBorderColor() { return null; }
}
