package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class NavBar {
	private final Style style;
	public NavBar(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
}
