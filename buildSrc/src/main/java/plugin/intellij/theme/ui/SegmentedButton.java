package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class SegmentedButton {
	private final Style style;
	SegmentedButton(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color focusedSelectedButtonColor() { return style.scheme().background().selectedText(); }
	@JsonProperty Color selectedButtonColor() { return style.theme().background().selected(); }
	@JsonProperty Color selectedEndBorderColor() { return style.theme().borderColor(); }
	@JsonProperty Color selectedStartBorderColor() { return style.theme().borderColor(); }
}
