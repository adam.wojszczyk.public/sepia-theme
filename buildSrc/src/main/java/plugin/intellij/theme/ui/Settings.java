package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Settings {
	private final Style style;
	Settings(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Spotlight Spotlight() { return new Spotlight(style); }

	static class Spotlight {
		private final Style style;
		Spotlight(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color borderColor() { return style.theme().background().search(); }
	}
}
