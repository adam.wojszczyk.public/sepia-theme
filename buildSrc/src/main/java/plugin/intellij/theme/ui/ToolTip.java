package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class ToolTip { // hover balloon
	private final Style style;
	ToolTip(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color infoForeground() { return style.theme().foreground().info(); }
	@JsonProperty Boolean paintBorder() { return true; }
	@JsonProperty Color shortcutForeground() { return style.theme().foreground().disabled(); }

	@JsonProperty Actions Actions() { return new Actions(style, this); }

	static class Actions {
		private final Style style;
		private final ToolTip toolTip;
		Actions(Style style, ToolTip toolTip) {
			this.style = requireNonNull(style);
			this.toolTip = requireNonNull(toolTip);
		}

		@JsonProperty Color background() { return toolTip.background().darker(); }
		@JsonProperty Color infoForeground() { return style.theme().foreground().info(); }
	}
}
