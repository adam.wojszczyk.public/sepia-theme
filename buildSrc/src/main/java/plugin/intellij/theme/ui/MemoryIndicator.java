package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class MemoryIndicator {
	private final Style style;
	MemoryIndicator(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color allocatedBackground() { return style.theme().background().hover(); }
	@JsonProperty Color usedBackground() { return style.theme().background().selected(); }
}
