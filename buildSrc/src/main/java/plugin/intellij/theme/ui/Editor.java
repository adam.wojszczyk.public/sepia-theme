package plugin.intellij.theme.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

class Editor {
	private final Style style;
	Editor(Style style) { this.style = requireNonNull(style); }

	@JsonProperty Color background() { return style.theme().background().base(); }
	@JsonProperty Color foreground() { return style.theme().foreground().base(); }
	@JsonProperty Color shortcutForeground() { return style.link(); }
	@JsonProperty SearchField SearchField() { return new SearchField(style); }
	@JsonProperty Toolbar Toolbar() { return new Toolbar(style); }
	@JsonProperty ToolTip ToolTip() { return new ToolTip(style); }

	static class SearchField {
		private final Style style;
		public SearchField(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.scheme().background().base(); }
	}

	static class Toolbar {
		private final Style style;
		public Toolbar(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color borderColor() { return style.theme().borderColor(); }
	}

	static class ToolTip {
		private final Style style;
		public ToolTip(Style style) { this.style = requireNonNull(style); }

		@JsonProperty Color background() { return style.theme().background().base(); }
		@JsonProperty Color border() { return style.theme().borderColor(); }
		@JsonProperty Color errorBackground() { return style.theme().background().base(); }
		@JsonProperty Color errorBorder() { return style.theme().borderColor(); }
		@JsonProperty Color foreground() { return style.theme().foreground().base(); }
		@JsonProperty Color selectionBackground() { return style.theme().background().selected(); }
		@JsonProperty Color successBackground() { return style.theme().background().base(); }
		@JsonProperty Color successBorder() { return style.theme().borderColor(); }
		@JsonProperty Color warningBackground() { return style.theme().background().base(); }
		@JsonProperty Color warningBorder() { return style.theme().borderColor(); }
	}
}
