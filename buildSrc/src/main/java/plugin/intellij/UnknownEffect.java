package plugin.intellij;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@Target(value= {TYPE, METHOD, FIELD})
@Retention(value= SOURCE)
public @interface UnknownEffect {
	String value() default "";
}
