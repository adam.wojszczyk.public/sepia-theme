package plugin.intellij.scheme;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static java.util.Objects.requireNonNull;

public interface Option {

	@JacksonXmlProperty(isAttribute = true)
	String name();

	@JacksonXmlProperty(isAttribute = true)
	String value();

	class Number implements Option {
		private final String name;
		private final int number;

		Number(String name, int number) {
			this.name = requireNonNull(name);
			this.number = number;
		}

		@JacksonXmlProperty(isAttribute = true)
		public String name() {
			return name;
		}

		@JacksonXmlProperty(isAttribute = true)
		public String value() {
			return Integer.toString(number);
		}
	}

	class Color implements Option {
		private final String name;
		private final plugin.model.color.Color color;

		public Color(String name, plugin.model.color.Color value) {
			this.name = requireNonNull(name);
			this.color = requireNonNull(value);
		}

		@JacksonXmlProperty(isAttribute = true) public String name() { return name; }
		@JacksonXmlProperty(isAttribute = true) public String value() { return color.asString(); }
	}
}
