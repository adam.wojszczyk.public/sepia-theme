package plugin.intellij.scheme;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import plugin.intellij.scheme.root.Root;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static java.util.Objects.requireNonNull;

@JsonPropertyOrder({"metaInfo", "colors", "attributes"})
@JacksonXmlRootElement(localName = "scheme")
public class Scheme {
	private final String name;
	private final Group root;

	public Scheme(String name, Style style, Palette palette) {
		this.name = requireNonNull(name);
		this.root = new Root(style, palette);
	}

	@JacksonXmlProperty(isAttribute = true)
	String name() {
		return name;
	}

	@JacksonXmlProperty(isAttribute = true)
	String version() {
		return "142";
	}

	@JacksonXmlProperty(isAttribute = true)
	String parent_scheme() {
		return "Default";
	}

	@JacksonXmlProperty
	MetaInfo metaInfo() {
		return new MetaInfo(name);
	}

	@JacksonXmlProperty
	Colors colors() {
		return new Colors(root.colors());
	}

	@JacksonXmlProperty
	Attributes attributes() {
		return new Attributes(root.attributes());
	}

	static class MetaInfo {

		private final String name;

		MetaInfo(String name) {
			this.name = requireNonNull(name);
		}

		@JacksonXmlElementWrapper(useWrapping = false)
		List<Property> property() {
			return List.of(
				new Property("ide", "Idea"),
				new Property("ideVersion", "2020.3.1.0.0"),
				new Property("originalScheme", name)
			);
		}
	}

	static class Colors {

		private final Set<Option.Color> colors;

		Colors(Set<Option.Color> colors) {
			this.colors = requireNonNull(colors);
		}

		@JacksonXmlElementWrapper(useWrapping = false)
		List<Option.Color> option() {
			return colors.stream()
				.sorted(Comparator.comparing(Option.Color::name))
				.toList();
		}
	}

	static class Attributes {

		private final Set<Attribute> attributes;

		Attributes(Set<Attribute> attributes) {
			this.attributes = requireNonNull(attributes);
		}

		@JacksonXmlElementWrapper(useWrapping = false)
		public List<Attribute> option() {
			return attributes.stream()
				.sorted(Comparator.comparing(Attribute::name))
				.toList();
		}
	}
}
