package plugin.intellij.scheme.root.general;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.intellij.scheme.root.Root;
import plugin.intellij.scheme.root.general.editor.Editor;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

public class General extends Group {

	private final Code code;
	private final Editor editor;
	private final ErrorsAndWarnings errorsAndWarnings;
	private final Hyperlinks hyperlinks;
	private final PopupsAndHints popupsAndHints;
	private final SearchResults searchResults;
	private final Text text;
	private final Group children;

	public General(Root root, Style style, Palette palette) {
		this.code = new Code(style, palette);
		this.editor = new Editor(style, palette);
		this.errorsAndWarnings = new ErrorsAndWarnings(root, style, palette);
		this.hyperlinks = new Hyperlinks(style, palette);
		this.popupsAndHints = new PopupsAndHints(style);
		this.searchResults = new SearchResults(root, style);
		this.text = new Text(style, palette);
		this.children = new Union(code, editor, errorsAndWarnings, hyperlinks, popupsAndHints, searchResults, text);
	}

	@Override
	public Set<Option.Color> colors() {
		return children.colors();
	}

	@Override
	public Set<Attribute> attributes() {
		return children.attributes();
	}

	Code code() { return code; }
	Editor editor() { return editor; }
	public ErrorsAndWarnings errorsAndWarnings() { return errorsAndWarnings; }
	Hyperlinks hyperlinks() { return hyperlinks; }
	PopupsAndHints popupsAndHints() { return popupsAndHints; }
	SearchResults searchResults() { return searchResults; }
	Text text() { return text; }
}
