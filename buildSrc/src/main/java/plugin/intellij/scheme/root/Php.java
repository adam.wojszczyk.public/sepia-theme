package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;

import java.util.Set;

class Php extends Group {

	private final Root root;
	private final Palette palette;

	Php(Root root, Palette palette) {
		this.root = root;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			magic_member_access(),
			doc_parameter(),
			exec_command_id(),
			named_argument(),
			parameter(),
			var()
		);
	}

	Attribute magic_member_access() {
		return new Attribute("MAGIC_MEMBER_ACCESS")
			.copy(root.general().errorsAndWarnings().unused_code());
	}

	Attribute doc_parameter() {
		return new Attribute("PHP_DOC_PARAMETER")
			.copy(root.languageDefaults().doc_comment_tag_value());
	}

	Attribute exec_command_id() {
		return new Attribute("PHP_EXEC_COMMAND_ID")
			.background(palette.silver().brighter(3));
	}

	Attribute named_argument() {
		return new Attribute("PHP_NAMED_ARGUMENT")
			.copy(root.languageDefaults().local_variable());
	}

	Attribute parameter() {
		return new Attribute("PHP_PARAMETER")
			.baseAttributes("DEFAULT_PARAMETER");
	}

	Attribute var() {
		return new Attribute("PHP_VAR")
			.baseAttributes("DEFAULT_LOCAL_VARIABLE");
	}
}
