package plugin.intellij.scheme.root.general;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

class Text extends Group {

	private final Style style;
	private final Palette palette;

	Text(Style style, Palette palette) {
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of(
			new Option.Color("READONLY_BACKGROUND", style.scheme().background().readOnly()),
			new Option.Color("FOLDED_TEXT_BORDER_COLOR", palette.yellow().brighter())
		);
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(text(), deleted_text_attributes(), folded_text_attributes());
	}

	Attribute text() {
		return new Attribute("TEXT")
			.foreground(style.scheme().foreground().base())
			.background(style.scheme().background().base());
	}

	Attribute deleted_text_attributes() {
		return new Attribute("DELETED_TEXT_ATTRIBUTES")
			.errorStripe(style.error())
			.dottedLine(style.error());
	}

	Attribute folded_text_attributes() {
		return new Attribute("FOLDED_TEXT_ATTRIBUTES")
			.foreground(palette.gray())
			.bordered(palette.gray().brighter());
	}
}
