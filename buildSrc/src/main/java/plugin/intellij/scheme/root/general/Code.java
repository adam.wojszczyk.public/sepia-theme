package plugin.intellij.scheme.root.general;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

import static java.util.Objects.requireNonNull;

class Code extends Group {
	private final Style style;
	private final Palette palette;

	Code(Style style, Palette palette) {
		this.style = requireNonNull(style);
		this.palette = requireNonNull(palette);
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of(
			new Option.Color("LINE_NUMBERS_COLOR", style.scheme().lines().darker(2))
		);
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			identifier_under_caret_attributes(), write_identifier_under_caret_attributes(), injected_language_fragment(), todo_default_attributes(),
			matched_brace_attributes(), unmatched_brace_attributes(),

			live_template_attributes(), live_template_inactive_segment(), template_variable_attributes()
		);
	}

	Attribute identifier_under_caret_attributes() {
		return new Attribute("IDENTIFIER_UNDER_CARET_ATTRIBUTES")
			.background(style.scheme().background().underCaret())
			.errorStripe(style.scheme().background().underCaret());
	}

	Attribute write_identifier_under_caret_attributes() {
		return new Attribute("WRITE_IDENTIFIER_UNDER_CARET_ATTRIBUTES")
			.background(style.scheme().background().underCaretWrite())
			.errorStripe(style.scheme().background().underCaretWrite().darker());
	}

	Attribute injected_language_fragment() {
		return new Attribute("INJECTED_LANGUAGE_FRAGMENT")
			.foreground(style.scheme().foreground().base().darker());
	}

	Attribute todo_default_attributes() {
		return new Attribute("TODO_DEFAULT_ATTRIBUTES")
			.foreground(style.scheme().foreground().base())
			.background(palette.orange().brighter()).errorStripeAsBackground();
	}

	Attribute matched_brace_attributes() {
		return new Attribute("MATCHED_BRACE_ATTRIBUTES")
			.foreground(style.scheme().foreground().base()).bold();
	}

	Attribute unmatched_brace_attributes() {
		return new Attribute("UNMATCHED_BRACE_ATTRIBUTES")
			.background(style.error()).errorStripeAsBackground();
	}

	Attribute live_template_attributes() {
		return new Attribute("LIVE_TEMPLATE_ATTRIBUTES")
			.bordered(palette.red().brighter());
	}

	Attribute live_template_inactive_segment() {
		return new Attribute("LIVE_TEMPLATE_INACTIVE_SEGMENT")
			.foreground(style.scheme().foreground().disabled());
	}

	Attribute template_variable_attributes() {
		return new Attribute("TEMPLATE_VARIABLE_ATTRIBUTES")
			.foreground(palette.purple());
	}
}
