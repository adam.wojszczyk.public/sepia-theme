package plugin.intellij.scheme.root.general;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.intellij.scheme.root.Root;
import plugin.style.Style;

import java.util.Set;

class SearchResults extends Group {

	private final Root root;
	private final Style style;

	SearchResults(Root root, Style style) {
		this.root = root;
		this.style = style;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(search_result_attributes(), write_search_result_attributes(), text_search_result_attributes());
	}

	Attribute search_result_attributes() {
		return new Attribute("SEARCH_RESULT_ATTRIBUTES")
			.foreground(style.scheme().foreground().base())
			.background(root.common().searchBackground()).errorStripeAsBackground();
	}

	Attribute write_search_result_attributes() {
		return new Attribute("WRITE_SEARCH_RESULT_ATTRIBUTES")
			.foreground(style.scheme().foreground().base())
			.background(root.common().searchWriteBackground()).errorStripeAsBackground();
	}

	Attribute text_search_result_attributes() {
		return new Attribute("TEXT_SEARCH_RESULT_ATTRIBUTES")
			.foreground(style.scheme().foreground().base())
			.background(root.common().searchBackground()).errorStripeAsBackground();
	}
}
