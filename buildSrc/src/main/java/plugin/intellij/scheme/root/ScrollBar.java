package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.color.Color;
import plugin.style.Style;

import java.util.Set;

import static java.util.Objects.requireNonNull;

class ScrollBar extends Group {
	private final Style style;

	ScrollBar(Style style) {
		this.style = requireNonNull(style);
	}

	@Override
	public Set<Option.Color> colors() {
		// intellij bug:
		// To make track transparent value needs to be empty
		// otherwise intellij cuts first and last sign of a value.
		//
		// Reproduction steps:
		// 1. apply plugin
		// 2. change something (vertical scrollbar colors) in scheme from settings,
		// 3. reset scheme to default
		// 4. restart ide
		return Set.of(
			new Option.Color("ScrollBar.Transparent.trackColor", Color.empty()),
			new Option.Color("ScrollBar.Transparent.thumbColor", style.scrollBar().thumbColor()),
			new Option.Color("ScrollBar.Transparent.thumbBorderColor", style.scrollBar().thumbBorderColor()),
			new Option.Color("ScrollBar.Transparent.hoverTrackColor", Color.empty()),
			new Option.Color("ScrollBar.Transparent.hoverThumbColor", style.scrollBar().hoverThumbColor()),
			new Option.Color("ScrollBar.Transparent.hoverThumbBorderColor", style.scrollBar().hoverThumbBorderColor()),

			new Option.Color("ScrollBar.Mac.Transparent.trackColor", Color.empty()),
			new Option.Color("ScrollBar.Mac.Transparent.thumbColor", style.scrollBar().thumbColor()),
			new Option.Color("ScrollBar.Mac.Transparent.thumbBorderColor", style.scrollBar().thumbBorderColor()),
			new Option.Color("ScrollBar.Mac.Transparent.hoverTrackColor", Color.empty()),
			new Option.Color("ScrollBar.Mac.Transparent.hoverThumbColor", style.scrollBar().hoverThumbColor()),
			new Option.Color("ScrollBar.Mac.Transparent.hoverThumbBorderColor", style.scrollBar().hoverThumbBorderColor())

			);
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of();
	}
}
