package plugin.intellij.scheme.root.vcs;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.color.Color;
import plugin.style.Style;

import java.util.Set;

import static java.util.Objects.requireNonNull;

public class VSC extends Group {

	private final Style style;
	private final Group children;

	public VSC(Style style) {
		this.style = requireNonNull(style);
		this.children = new Group.Union(new VcsAnnotations(this.style));
	}

	@Override
	public Set<Option.Color> colors() {
		Color added = style.scheme().changes().added().brighter();
		Color modified = style.scheme().changes().modified().brighter();
		Color deleted = style.scheme().changes().deleted();
		return union(
			Set.of(
				new Option.Color("ADDED_LINES_COLOR", added),
				new Option.Color("IGNORED_ADDED_LINES_BORDER_COLOR", added),
				new Option.Color("MODIFIED_LINES_COLOR", modified),
				new Option.Color("IGNORED_MODIFIED_LINES_BORDER_COLOR", modified),
				new Option.Color("WHITESPACES_MODIFIED_LINES_COLOR", modified.brighter()),
				new Option.Color("DELETED_LINES_COLOR", deleted),
				new Option.Color("IGNORED_DELETED_LINES_BORDER_COLOR", deleted)
			),
			children.colors()
		);
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of();
	}
}
