package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

class Python extends Group {
	private final Root root;
	private final Style style;
	private final Palette palette;

	Python(Root root, Style style, Palette palette) {
		this.root = root;
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			decorator(),
			keyword_argument(),
			predefined_definition(),
			predefined_usage(),
			self_parameter(),
			string_u()
		);
	}

	Attribute decorator() {
		return new Attribute("PY.DECORATOR")
			.copy(root.languageDefaults().metadata());
	}

	Attribute keyword_argument() {
		return new Attribute("PY.KEYWORD_ARGUMENT")
			.copy(root.languageDefaults().local_variable());
	}

	Attribute predefined_definition() {
		return new Attribute("PY.PREDEFINED_DEFINITION")
			.baseAttributes("DEFAULT_PREDEFINED_SYMBOL");
	}

	Attribute predefined_usage() {
		return new Attribute("PY.PREDEFINED_USAGE")
			.baseAttributes("DEFAULT_PREDEFINED_SYMBOL");
	}

	Attribute self_parameter() {
		return new Attribute("PY.SELF_PARAMETER")
			.baseAttributes("DEFAULT_PARAMETER");
	}

	Attribute string_u() {
		return new Attribute("PY.STRING.U")
			.baseAttributes("DEFAULT_STRING");
	}
}
