package plugin.intellij.scheme.root.general.editor;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

import static java.util.Objects.requireNonNull;

public class Editor extends Group {

	private final Style style;
	private final Palette palette;
	private final Guides children;

	public Editor(Style style, Palette palette) {
		this.style = requireNonNull(style);
		this.palette = requireNonNull(palette);
		this.children = new Guides(style);
	}

	@Override
	public Set<Option.Color> colors() {
		return union(
			Set.of(
				new Option.Color("CARET_COLOR", style.scheme().foreground().base()),
				new Option.Color("CARET_ROW_COLOR", style.scheme().background().selectedLine()),
				new Option.Color("SELECTION_FOREGROUND", style.scheme().foreground().selectedText()),
				new Option.Color("SELECTION_BACKGROUND", style.scheme().background().selectedText()),
				new Option.Color("GUTTER_BACKGROUND", style.scheme().background().base()),
				new Option.Color("TEARLINE_COLOR", style.scheme().lines().darker()),
				new Option.Color("SELECTED_TEARLINE_COLOR", style.scheme().foreground().base())
			),
			children.colors()
		);
	}

	@Override
	public Set<Attribute> attributes() {
		return union(
			Set.of(
				bookmarks_attributes(), default_attribute(),

				inlay_default(),
				inlay_text_without_background(),
				inline_parameter_hint(), inline_parameter_hint_current(), inline_parameter_hint_highlighted(),

				breakpoint_attributes()
			),
			new Breadcrumbs(style).attributes()
		);
	}

	Attribute bookmarks_attributes() {
		return new Attribute("BOOKMARKS_ATTRIBUTES")
			.errorStripe(style.scheme().foreground().base());
	}

	Attribute default_attribute() {
		return new Attribute("DEFAULT_ATTRIBUTE")
			.foreground(palette.purple().darker());
	}

	Attribute inlay_text_without_background() {
		return new Attribute("INLAY_TEXT_WITHOUT_BACKGROUND")
			.foreground(style.scheme().foreground().inlayHintWithoutBackground());
	}

	Attribute inlay_default() {
		return new Attribute("INLAY_DEFAULT")
			.foreground(style.scheme().foreground().inlayHint())
			.background(style.scheme().background().inlayHint());
	}

	Attribute inline_parameter_hint() {
		return new Attribute("INLINE_PARAMETER_HINT")
			.foreground(style.scheme().foreground().inlayHint())
			.background(style.scheme().background().inlayHint());
	}

	Attribute inline_parameter_hint_current() {
		return new Attribute("INLINE_PARAMETER_HINT_CURRENT")
			.foreground(style.scheme().foreground().inlayCurrentHint())
			.background(style.scheme().background().inlayHint());
	}

	Attribute inline_parameter_hint_highlighted() {
		return new Attribute("INLINE_PARAMETER_HINT_HIGHLIGHTED")
			.foreground(style.scheme().foreground().inlayHighlightedHint())
			.background(style.scheme().background().inlayHint());
	}

	Attribute breakpoint_attributes() {
		return new Attribute("BREAKPOINT_ATTRIBUTES")
			.background(style.scheme().background().base().darker(3));
	}
}
