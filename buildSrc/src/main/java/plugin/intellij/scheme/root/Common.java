package plugin.intellij.scheme.root;

import plugin.model.color.Color;
import plugin.style.Style;

import static java.util.Objects.requireNonNull;

public class Common {
	private final Style style;

	Common(Style style) {
		this.style = requireNonNull(style);
	}

	public Color searchBackground() {
		return style.theme().background().search();
	}

	public Color searchWriteBackground() {
		return searchBackground().darker();
	}
}
