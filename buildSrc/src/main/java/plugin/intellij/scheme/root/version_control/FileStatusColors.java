package plugin.intellij.scheme.root.version_control;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

public class FileStatusColors extends Group {

	private final Style style;
	private final Palette palette;

	public FileStatusColors(Style style, Palette palette) {
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of(
			new Option.Color("FILESTATUS_NOT_CHANGED", style.theme().foreground().base()),
			new Option.Color("FILESTATUS_ADDED", style.scheme().changes().addedFilestatus()),
			new Option.Color("FILESTATUS_addedOutside", style.scheme().changes().addedFilestatus().brighter()),
			new Option.Color("FILESTATUS_NOT_CHANGED_RECURSIVE", style.scheme().changes().modified().darker()),
			new Option.Color("FILESTATUS_NOT_CHANGED_IMMEDIATE", style.scheme().changes().modified().darker()),
			new Option.Color("FILESTATUS_MODIFIED", style.scheme().changes().modified().darker()),
			new Option.Color("FILESTATUS_modifiedOutside", style.scheme().changes().modified().darker()),
			new Option.Color("FILESTATUS_DELETED", style.theme().foreground().info().brighter(2)),
			new Option.Color("FILESTATUS_IDEA_FILESTATUS_DELETED_FROM_FILE_SYSTEM", style.theme().foreground().info().brighter(2)),
			new Option.Color("FILESTATUS_IDEA_FILESTATUS_IGNORED", style.theme().foreground().disabled()),
			new Option.Color("FILESTATUS_IDEA_FILESTATUS_MERGED_WITH_BOTH_CONFLICTS", palette.red()),
			new Option.Color("FILESTATUS_IDEA_FILESTATUS_MERGED_WITH_CONFLICTS", palette.red()),
			new Option.Color("FILESTATUS_IDEA_FILESTATUS_MERGED_WITH_PROPERTY_CONFLICTS", palette.red()),
			new Option.Color("FILESTATUS_changelistConflict", palette.red()),
			new Option.Color("FILESTATUS_MERGED", palette.red().brighter(2)),
			new Option.Color("FILESTATUS_OBSOLETE", palette.orange()),
			// new Option.Color("FILESTATUS_SUPPRESSED", null),
			new Option.Color("FILESTATUS_SWITCHED", palette.aqua().darker()),
			new Option.Color("FILESTATUS_UNKNOWN", palette.red().darker())
			// new Option.Color("FILESTATUS_HIJACKED", null),
		);
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of();
	}
}
