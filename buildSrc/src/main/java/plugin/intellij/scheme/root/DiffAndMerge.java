package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.model.color.Color;
import plugin.style.Style;

import java.util.Set;

class DiffAndMerge extends Group {
	private final Style style;
	private final Palette palette;

	DiffAndMerge(Style style, Palette palette) {
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of(
			new Option.Color("DIFF_SEPARATOR_WAVE", style.scheme().lines())
		);
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			new Attribute("DIFF_INSERTED")
				.background(palette.green().brighter().opacity(0.3))
				.background(new Color("e7e197"))
				.foreground(palette.green().brighter().opacity(0.08))
				.foreground(new Color("f5edba"))
				.errorStripe(palette.green().brighter()),
			new Attribute("DIFF_MODIFIED")
				.background(palette.blue().brighter().opacity(0.3))
				.background(new Color("d7dab9"))
				.foreground(palette.blue().brighter().opacity(0.08))
				.foreground(new Color(("f1ebc3")))
				.errorStripe(palette.blue().brighter()),
			new Attribute("DIFF_DELETED")
				.background(style.scheme().background().base().darker(4).opacity(0.6))
				.background(new Color("e4d6b1"))
				.foreground(style.scheme().background().base().darker(4).opacity(0.1))
				.foreground(new Color("f7ecc3"))
				.errorStripe(style.scheme().background().base().darker(4)),
			new Attribute("DIFF_CONFLICT")
				.background(palette.red().brighter().opacity(0.25))
				.background(new Color("fbc7a3"))
				.foreground(palette.red().brighter().opacity(0.03))
				.foreground(new Color("fbecc3"))
				.errorStripe(palette.red().brighter(2))
		);
	}
}
