package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

class Java extends Group {
	private final Root root;
	private final Style style;
	private final Palette palette;

	Java(Root root, Style style, Palette palette) {
		this.root = root;
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			abstract_class_name_attributes(),
			annotation_attribute_name_attributes(),
			annotation_name_attributes(),
			implicit_anonymous_class_parameter_attributes(),
			instance_field_attributes(),
			static_field_attributes(),
			static_final_field_attributes(),
			type_parameter_name_attributes()
		);
	}

	Attribute abstract_class_name_attributes() {
		return new Attribute("ABSTRACT_CLASS_NAME_ATTRIBUTES")
			.copy(root.languageDefaults().interface_name());
	}

	Attribute annotation_attribute_name_attributes() {
		return new Attribute("ANNOTATION_ATTRIBUTE_NAME_ATTRIBUTES")
			.foreground(palette.purple());
	}

	Attribute annotation_name_attributes() {
		return new Attribute("ANNOTATION_NAME_ATTRIBUTES")
			.baseAttributes("DEFAULT_METADATA");
	}

	Attribute implicit_anonymous_class_parameter_attributes() {
		return new Attribute("IMPLICIT_ANONYMOUS_CLASS_PARAMETER_ATTRIBUTES")
			.copy(root.languageDefaults().parameter());
	}

	Attribute instance_field_attributes() {
		return new Attribute("INSTANCE_FIELD_ATTRIBUTES")
			.baseAttributes("DEFAULT_INSTANCE_FIELD");
	}

	Attribute static_field_attributes() {
		return new Attribute("STATIC_FIELD_ATTRIBUTES")
			.baseAttributes("DEFAULT_STATIC_FIELD");
	}

	Attribute static_final_field_attributes() {
		return new Attribute("STATIC_FINAL_FIELD_ATTRIBUTES")
			.baseAttributes("STATIC_FIELD_ATTRIBUTES");
	}

	Attribute type_parameter_name_attributes() {
		return new Attribute("TYPE_PARAMETER_NAME_ATTRIBUTES")
			.foreground(style.scheme().foreground().base()).bold();
	}
}
