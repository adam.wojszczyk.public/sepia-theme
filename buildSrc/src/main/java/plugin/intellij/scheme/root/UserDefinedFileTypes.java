package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.intellij.scheme.root.language_defaults.LanguageDefaults;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

class UserDefinedFileTypes extends Group {
	private final Style style;
	private final Palette palette;

	UserDefinedFileTypes(Style style, Palette palette) {
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			invalid_string_escape_attributes(),
			keyword1_attributes(),
			keyword2_attributes(),
			keyword3_attributes(),
			keyword4_attributes(),
			line_comment_attributes(),
			multi_line_comment_attributes(),
			number_attributes(),
			string_attributes(),
			valid_string_escape_attributes()
		);
	}

	Attribute invalid_string_escape_attributes() {
		return new Attribute("CUSTOM_INVALID_STRING_ESCAPE_ATTRIBUTES")
			.baseAttributes("DEFAULT_INVALID_STRING_ESCAPE");
	}

	Attribute keyword1_attributes() {
		return new Attribute("CUSTOM_KEYWORD1_ATTRIBUTES")
			.baseAttributes("DEFAULT_KEYWORD");
	}

	Attribute keyword2_attributes() {
		return new Attribute("CUSTOM_KEYWORD2_ATTRIBUTES")
			.copy(new LanguageDefaults(style, palette).parameter());
	}

	Attribute keyword3_attributes() {
		return new Attribute("CUSTOM_KEYWORD3_ATTRIBUTES")
			.copy(new LanguageDefaults(style, palette).parameter());
	}

	Attribute keyword4_attributes() {
		return new Attribute("CUSTOM_KEYWORD4_ATTRIBUTES")
			.copy(new LanguageDefaults(style, palette).parameter());
	}

	Attribute line_comment_attributes() {
		return new Attribute("CUSTOM_LINE_COMMENT_ATTRIBUTES")
			.baseAttributes("DEFAULT_LINE_COMMENT");
	}

	Attribute multi_line_comment_attributes() {
		return new Attribute("CUSTOM_MULTI_LINE_COMMENT_ATTRIBUTES")
			.baseAttributes("DEFAULT_BLOCK_COMMENT");
	}

	Attribute number_attributes() {
		return new Attribute("CUSTOM_NUMBER_ATTRIBUTES")
			.baseAttributes("DEFAULT_NUMBER");
	}

	Attribute string_attributes() {
		return new Attribute("CUSTOM_STRING_ATTRIBUTES")
			.baseAttributes("DEFAULT_STRING");
	}

	Attribute valid_string_escape_attributes() {
		return new Attribute("CUSTOM_VALID_STRING_ESCAPE_ATTRIBUTES")
			.baseAttributes("DEFAULT_VALID_STRING_ESCAPE");
	}
}
