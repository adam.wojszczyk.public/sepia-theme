package plugin.intellij.scheme.root.general;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.intellij.scheme.root.Root;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

import static java.util.Objects.requireNonNull;

public class ErrorsAndWarnings extends Group {

	private final Root root;
	private final Style style;
	private final Palette palette;

	ErrorsAndWarnings(Root root, Style style, Palette palette) {
		this.root = root;
		this.style = requireNonNull(style);
		this.palette = requireNonNull(palette);
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			deprecated_symbol(),
			deprecated_symbol_marked_for_removal(),
			duplicate_from_server(),
			error(),
			grammar_error(),
			problem_from_server(),
			runtime_problem(),
			text_style_error(),
			text_style_suggestion(),
			text_style_warning(),
			typo(),
			unknown_symbol(),
			unused_code(),
			warning(),
			weak_warning()
		);
	}

	Attribute deprecated_symbol() {
		return new Attribute("DEPRECATED_ATTRIBUTES")
			.strikeout(style.scheme().foreground().deprecated());
	}

	Attribute deprecated_symbol_marked_for_removal() {
		return new Attribute("MARKED_FOR_REMOVAL_ATTRIBUTES")
			.copy(deprecated_symbol());
	}

	Attribute duplicate_from_server() {
		return new Attribute("DUPLICATE_FROM_SERVER")
			.boldUnderscored(style.scheme().warning());
	}

	Attribute error() {
		return new Attribute("ERRORS_ATTRIBUTES")
			.underwaved(style.error())
			.errorStripeAsEffect();
	}

	Attribute grammar_error() {
		return new Attribute("GRAMMAR_ERROR")
			.copy(typo());
	}

	Attribute problem_from_server() {
		return new Attribute("GENERIC_SERVER_ERROR_OR_WARNING")
			.boldUnderscored(style.scheme().warning());
	}

	Attribute runtime_problem() {
		return new Attribute("RUNTIME_ERROR")
			.boldUnderscored(style.error());
	}

	Attribute text_style_error() {
		return new Attribute("TEXT_STYLE_ERROR")
			.copy(typo());
	}

	Attribute text_style_suggestion() {
		return new Attribute("TEXT_STYLE_SUGGESTION")
			.copy(typo());
	}

	Attribute text_style_warning() {
		return new Attribute("TEXT_STYLE_WARNING")
			.copy(typo());
	}

	Attribute typo() {
		return new Attribute("TYPO")
			.dottedLine(style.scheme().foreground().unused());
	}

	Attribute unknown_symbol() {
		return new Attribute("WRONG_REFERENCES_ATTRIBUTES")
			.underwaved(style.error())
			.errorStripeAsEffect();
	}

	public Attribute unused_code() {
		return new Attribute("NOT_USED_ELEMENT_ATTRIBUTES")
			.foreground(style.scheme().foreground().unused());
	}

	Attribute warning() {
		return new Attribute("WARNING_ATTRIBUTES")
			.underwaved(style.scheme().warning())
			.errorStripeAsEffect();
	}

	Attribute weak_warning() {
		return new Attribute("INFO_ATTRIBUTES")
			.underwaved(style.scheme().weak_warning())
			.errorStripeAsEffect();
	}
}
