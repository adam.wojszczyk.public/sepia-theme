package plugin.intellij.scheme.root.sonar_lint;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.style.Style;

import java.util.Set;

import static java.util.Objects.requireNonNull;

public class SonarLint extends Group {

	private final Style style;

	public SonarLint(Style style) {
		this.style = requireNonNull(style);
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			blocker(),
			critical(),
			major(),
			minor(),
			info(),
			selected()
		);
	}

	Attribute blocker() {
		return new Attribute("SONARLINT_BLOCKER");
	}

	Attribute critical() {
		return new Attribute("SONARLINT_CRITICAL");
	}

	Attribute major() {
		return new Attribute("SONARLINT_MAJOR");
	}

	Attribute minor() {
		return new Attribute("SONARLINT_MINOR");
	}

	Attribute info() {
		return new Attribute("SONARLINT_INFO");
	}

	Attribute selected() {
		return new Attribute("SONARLINT_SELECTED")
			.bordered(style.scheme().warning());
	}
}
