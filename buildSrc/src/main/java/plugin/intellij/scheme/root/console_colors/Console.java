package plugin.intellij.scheme.root.console_colors;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

class Console extends Group {

	private final Style style;
	private final Palette palette;

	Console(Style style, Palette palette) {
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of(
			new Option.Color("CONSOLE_BACKGROUND_KEY", style.scheme().background().base())
		);
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(error(), system(), user_input(), normal());
	}

	Attribute error() {
		return new Attribute("CONSOLE_ERROR_OUTPUT")
			.foreground(style.error());
	}

	Attribute system() {
		return new Attribute("CONSOLE_SYSTEM_OUTPUT")
			.foreground(palette.yellow().darker());
	}

	Attribute user_input() {
		return new Attribute("CONSOLE_USER_INPUT")
			.foreground(style.scheme().foreground().base());
	}

	Attribute normal() {
		return new Attribute("CONSOLE_NORMAL_OUTPUT")
			.foreground(style.scheme().foreground().base());
	}
}
