package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.intellij.scheme.root.console_colors.ConsoleColors;
import plugin.intellij.scheme.root.general.General;
import plugin.intellij.scheme.root.language_defaults.LanguageDefaults;
import plugin.intellij.scheme.root.sonar_lint.SonarLint;
import plugin.intellij.scheme.root.vcs.VSC;
import plugin.intellij.scheme.root.version_control.FileStatusColors;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

public class Root extends Group {

	private final General general;
	private final LanguageDefaults languageDefaults;

	private final Group children;
	private final Java java;
	private final Common common;

	public Root(Style style, Palette palette) {
		this.common = new Common(style);
		this.general = new General(this, style, palette);
		this.languageDefaults = new LanguageDefaults(style, palette);
		this.java = new Java(this, style, palette);
		this.children = new Group.Union(
			general,
			languageDefaults,
			new ConsoleColors(style, palette),
			new DiffAndMerge(style, palette),
			java,
			new Groovy(this),
			new Go(this, palette),
			new TypeScript(this, style, palette),
			new JavaScript(this, style, palette),
			new Python(this, style, palette),
			new Php(this, palette),
			new UserDefinedFileTypes(style, palette),
			new FileStatusColors(style, palette),
			new VSC(style),
			new ScrollBar(style),
			new SonarLint(style)
		);
	}

	public Common common() {
		return common;
	}

	public General general() {
		return general;
	}

	public LanguageDefaults languageDefaults() {
		return languageDefaults;
	}

	public Java java() {
		return java;
	}

	@Override
	public Set<Option.Color> colors() {
		return children.colors();
	}

	@Override
	public Set<Attribute> attributes() {
		return children.attributes();
	}
}
