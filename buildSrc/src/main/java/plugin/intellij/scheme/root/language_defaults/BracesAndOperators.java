package plugin.intellij.scheme.root.language_defaults;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.style.Style;

import java.util.Set;

class BracesAndOperators extends Group {

	private final Style style;

	BracesAndOperators(Style style) {
		this.style = style;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(braces(), brackets(), comma(), dot(), operation_sign(), parenths(), semicolon());
	}

	public Attribute braces() {
		return new Attribute("DEFAULT_BRACES")
			.foreground(style.scheme().foreground().base());
	}

	public Attribute brackets() {
		return new Attribute("DEFAULT_BRACKETS")
			.foreground(style.scheme().foreground().base());
	}

	public Attribute comma() {
		return new Attribute("DEFAULT_COMMA")
			.foreground(style.scheme().foreground().base());
	}

	public Attribute dot() {
		return new Attribute("DEFAULT_DOT")
			.foreground(style.scheme().foreground().base());
	}

	public Attribute operation_sign() {
		return new Attribute("DEFAULT_OPERATION_SIGN")
			.foreground(style.scheme().foreground().base());
	}

	public Attribute parenths() {
		return new Attribute("DEFAULT_PARENTHS")
			.foreground(style.scheme().foreground().base());
	}

	public Attribute semicolon() {
		return new Attribute("DEFAULT_SEMICOLON")
			.foreground(style.scheme().foreground().base());
	}
}
