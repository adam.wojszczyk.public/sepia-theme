package plugin.intellij.scheme.root.console_colors;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

class ANSIColors extends Group {

	private final Style style;
	private final Palette palette;

	ANSIColors(Style style, Palette palette) {
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			black(), blue_bright(), blue(), cyan_bright(), cyan(), darkgray(), gray(), green_bright(), green(),
			magenta_bright(), magenta(), red_bright(), red(), white(), yellow_bright(), yellow()
		);
	}

	Attribute black() {
		return new Attribute("CONSOLE_BLACK_OUTPUT")
			.foreground(style.scheme().foreground().base());
	}

	Attribute blue_bright() {
		return new Attribute("CONSOLE_BLUE_BRIGHT_OUTPUT")
			.foreground(palette.blue().brighter());
	}

	Attribute blue() {
		return new Attribute("CONSOLE_BLUE_OUTPUT")
			.foreground(palette.blue());
	}

	Attribute cyan_bright() {
		return new Attribute("CONSOLE_CYAN_BRIGHT_OUTPUT")
			.foreground(palette.aqua().brighter());
	}

	Attribute cyan() {
		return new Attribute("CONSOLE_CYAN_OUTPUT")
			.foreground(palette.aqua());
	}

	Attribute darkgray() {
		return new Attribute("CONSOLE_DARKGRAY_OUTPUT")
			.foreground(palette.gray().darker());
	}

	Attribute gray() {
		return new Attribute("CONSOLE_GRAY_OUTPUT")
			.foreground(palette.gray());
	}

	Attribute green_bright() {
		return new Attribute("CONSOLE_GREEN_BRIGHT_OUTPUT")
			.foreground(palette.green().brighter());
	}

	Attribute green() {
		return new Attribute("CONSOLE_GREEN_OUTPUT")
			.foreground(palette.green());
	}

	Attribute magenta_bright() {
		return new Attribute("CONSOLE_MAGENTA_BRIGHT_OUTPUT")
			.foreground(palette.purple().brighter());
	}

	Attribute magenta() {
		return new Attribute("CONSOLE_MAGENTA_OUTPUT")
			.foreground(palette.purple());
	}

	Attribute red_bright() {
		return new Attribute("CONSOLE_RED_BRIGHT_OUTPUT")
			.foreground(palette.red().brighter());
	}

	Attribute red() {
		return new Attribute("CONSOLE_RED_OUTPUT")
			.foreground(palette.red());
	}

	Attribute white() {
		return new Attribute("CONSOLE_WHITE_OUTPUT")
			.foreground(style.scheme().foreground().base());
	}

	Attribute yellow_bright() {
		return new Attribute("CONSOLE_YELLOW_BRIGHT_OUTPUT")
			.foreground(palette.yellow().brighter());
	}

	Attribute yellow() {
		return new Attribute("CONSOLE_YELLOW_OUTPUT")
			.foreground(palette.yellow());
	}
}
