package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;

import java.util.Set;

class Go extends Group {
	private final Root root;
	private final Palette palette;

	Go(Root root, Palette palette) {
		this.root = root;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			builtin_constant(),
			builtin_variable(),
			comment_reference(),
			function_parameter(),
			local_constant(),
			method_receiver(),
			package_exported_constant(),
			package_exported_variable(),
			package_local_constant(),
			package_local_variable(),
			shadowing_variable(),
			struct_exported_member(),
			struct_local_member()
		);
	}

	Attribute builtin_constant() {
		return new Attribute("GO_BUILTIN_CONSTANT")
			.copy(root.languageDefaults().keyword());
	}

	Attribute builtin_variable() {
		return new Attribute("GO_BUILTIN_VARIABLE")
			.copy(root.languageDefaults().keyword());
	}

	Attribute comment_reference() {
		return new Attribute("GO_COMMENT_REFERENCE")
			.copy(root.languageDefaults().doc_comment_tag_value());
	}

	Attribute function_parameter() {
		return new Attribute("GO_FUNCTION_PARAMETER")
			.copy(root.languageDefaults().parameter());
	}

	Attribute local_constant() {
		return new Attribute("GO_LOCAL_CONSTANT")
			.foreground(palette.purple());
	}

	Attribute method_receiver() {
		return new Attribute("GO_METHOD_RECEIVER")
			.foreground(palette.purple().darker()).bold();
	}

	Attribute package_exported_constant() {
		return new Attribute("GO_PACKAGE_EXPORTED_CONSTANT")
			.foreground(palette.purple().darker()).bold();
	}

	Attribute package_exported_variable() {
		return new Attribute("GO_PACKAGE_EXPORTED_VARIABLE")
			.foreground(palette.purple().darker()).bold();
	}

	Attribute package_local_constant() {
		return new Attribute("GO_PACKAGE_LOCAL_CONSTANT")
			.foreground(palette.purple().darker()).bold();
	}

	Attribute package_local_variable() {
		return new Attribute("GO_PACKAGE_LOCAL_VARIABLE")
			.foreground(palette.purple().darker()).bold();
	}

	Attribute shadowing_variable() {
		return new Attribute("GO_SHADOWING_VARIABLE")
			.foreground(palette.purple()).dottedLine(palette.purple());
	}

	Attribute struct_exported_member() {
		return new Attribute("GO_STRUCT_EXPORTED_MEMBER")
			.copy(root.languageDefaults().instance_field());
	}

	Attribute struct_local_member() {
		return new Attribute("GO_STRUCT_LOCAL_MEMBER")
			.copy(root.languageDefaults().instance_field());
	}

}
