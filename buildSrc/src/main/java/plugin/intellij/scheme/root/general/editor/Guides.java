package plugin.intellij.scheme.root.general.editor;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.style.Style;

import java.util.Set;

class Guides extends Group {
	private final Style style;

	Guides(Style style) {
		this.style = style;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of(
			hardWrapGuide(),
			new Option.Color("SELECTED_INDENT_GUIDE", style.scheme().foreground().base()),
			new Option.Color("VISUAL_INDENT_GUIDE", style.scheme().background().base().darker(3)),
			new Option.Color("INDENT_GUIDE", style.scheme().lines())
		);
	}

	private Option.Color hardWrapGuide() {
		return new Option.Color("RIGHT_MARGIN_COLOR", style.scheme().lines());
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of();
	}
}
