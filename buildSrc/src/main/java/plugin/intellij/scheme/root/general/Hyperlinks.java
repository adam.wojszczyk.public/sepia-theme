package plugin.intellij.scheme.root.general;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

class Hyperlinks extends Group {
	private final Style style;
	private final Palette palette;

	Hyperlinks(Style style, Palette palette) {
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(hyperlink_attributes(), ctrl_clickable(), followed_hyperlink_attributes(), inactive_hyperlink_attributes());
	}

	public Attribute hyperlink_attributes() {
		return new Attribute("HYPERLINK_ATTRIBUTES")
			.foreground(style.link()).underscored(style.link());
	}

	public Attribute ctrl_clickable() {
		return new Attribute("CTRL_CLICKABLE")
			.foreground(style.link()).underscored(style.link());
	}

	public Attribute followed_hyperlink_attributes() {
		return new Attribute("FOLLOWED_HYPERLINK_ATTRIBUTES")
			.foreground(palette.purple().darker())
			.underscored(palette.purple().darker());
	}

	public Attribute inactive_hyperlink_attributes() {
		return new Attribute("INACTIVE_HYPERLINK_ATTRIBUTES")
			.foreground(style.scheme().foreground().base())
			.underscored(style.scheme().foreground().base());
	}
}
