package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

class JavaScript extends Group {
	private final Root root;
	private final Style style;
	private final Palette palette;

	JavaScript(Root root, Style style, Palette palette) {
		this.root = root;
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			global_variable(),
			global_function(),
			injected_language_fragment(),
			instance_member_function(),
			local_variable(),
			parameter(),
			regex()
		);
	}

	Attribute global_variable() {
		return new Attribute("JS.GLOBAL_VARIABLE")
			.baseAttributes("DEFAULT_PARAMETER");
	}

	Attribute global_function() {
		return new Attribute("JS.GLOBAL_FUNCTION")
			.baseAttributes("DEFAULT_FUNCTION_DECLARATION");
	}

	Attribute injected_language_fragment() {
		return new Attribute("JavaScript:INJECTED_LANGUAGE_FRAGMENT")
			.baseAttributes("INJECTED_LANGUAGE_FRAGMENT");
	}

	Attribute instance_member_function() {
		return new Attribute("JS.INSTANCE_MEMBER_FUNCTION")
			.baseAttributes("DEFAULT_INSTANCE_METHOD");
	}

	Attribute local_variable() {
		return new Attribute("JS.LOCAL_VARIABLE")
			.baseAttributes("DEFAULT_LOCAL_VARIABLE");
	}

	Attribute parameter() {
		return new Attribute("JS.PARAMETER")
			.baseAttributes("DEFAULT_PARAMETER");
	}

	Attribute regex() {
		return new Attribute("JS.REGEXP")
			.copy(root.languageDefaults().valid_string_escape());
	}
}
