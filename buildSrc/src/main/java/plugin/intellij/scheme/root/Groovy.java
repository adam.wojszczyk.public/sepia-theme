package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;

import java.util.Set;

class Groovy extends Group {
	private final Root root;

	Groovy(Root root) {
		this.root = root;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			groovy_keyword(),
			list_map_to_object_conversion(),
			static_property_reference_ID(),
			unresolved_reference_access()
		);
	}

	Attribute groovy_keyword() {
		return new Attribute("GROOVY_KEYWORD")
			.baseAttributes("JAVA_KEYWORD");
	}

	Attribute list_map_to_object_conversion() {
		return new Attribute("List/map to object conversion")
			.copy(root.languageDefaults().identifier());
	}

	Attribute static_property_reference_ID() {
		return new Attribute("Static property reference ID")
			.baseAttributes("STATIC_FINAL_FIELD_ATTRIBUTES");
	}

	Attribute unresolved_reference_access() {
		return new Attribute("Unresolved reference access")
			.copy(root.general().errorsAndWarnings().unused_code());
	}
}
