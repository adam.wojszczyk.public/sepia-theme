package plugin.intellij.scheme.root.language_defaults;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

public class LanguageDefaults extends Group {

	private final Style style;
	private final Palette palette;
	private final Group children;

	public LanguageDefaults(Style style, Palette palette) {
		this.style = style;
		this.palette = palette;
		this.children = new Group.Union(new BracesAndOperators(style));
	}

	@Override
	public Set<Option.Color> colors() {
		return children.colors();
	}

	@Override
	public Set<Attribute> attributes() {
		return union(
			children.attributes(),
			Set.of(
				bad_character(),
				string(), valid_string_escape(), invalid_string_escape(), constant(),

				line_comment(), block_comment(), doc_comment(), doc_comment_tag(), doc_comment_tag_value(), doc_markup(),

				entity(), function_declaration(), global_variable(), identifier(), instance_field(), interface_name(), keyword(), label(), local_variable(), metadata(), number(), parameter(),
				predefined_symbol(), reassigned_local_variable(), reassigned_parameter(), static_field(), static_method(), tag(), template_language_color()
			)
		);
	}

	Attribute bad_character() {
		return new Attribute("BAD_CHARACTER")
			.underwaved(style.error())
			.errorStripeAsEffect();
	}

	Attribute commonKeyword() {
		return new Attribute("")
			.foreground(palette.blue()).bold();
	}

	Attribute string() {
		return new Attribute("DEFAULT_STRING")
			.foreground(palette.aqua())
			.bold();
	}

	public Attribute valid_string_escape() {
		return new Attribute("DEFAULT_VALID_STRING_ESCAPE")
			.foreground(palette.aqua().darker())
			.bold();
	}

	Attribute invalid_string_escape() {
		return new Attribute("DEFAULT_INVALID_STRING_ESCAPE")
			.foreground(style.error()).underwaved(style.error())
			.bold();
	}

	Attribute constant() {
		return new Attribute("DEFAULT_CONSTANT")
			.foreground(palette.purple().darker(2))
			.bold();
	}

	Attribute line_comment() {
		return new Attribute("DEFAULT_LINE_COMMENT")
			.foreground(palette.gray())
			.italic();
	}

	Attribute block_comment() {
		return new Attribute("DEFAULT_BLOCK_COMMENT")
			.foreground(palette.gray())
			.italic();
	}

	Attribute doc_comment() {
		return new Attribute("DEFAULT_DOC_COMMENT")
			.foreground(palette.gray())
			.italic();
	}

	Attribute doc_comment_tag() {
		return new Attribute("DEFAULT_DOC_COMMENT_TAG")
			.foreground(palette.gray())
			.italic()
			.bold();
	}

	public Attribute doc_comment_tag_value() {
		return new Attribute("DEFAULT_DOC_COMMENT_TAG_VALUE")
			.foreground(palette.gray().darker(2))
			.bold();
	}

	Attribute doc_markup() {
		return new Attribute("DEFAULT_DOC_MARKUP")
			.copy(commonKeyword()).removeFontType();
	}

	Attribute entity() {
		return new Attribute("DEFAULT_ENTITY")
			.foreground(palette.yellow());
	}

	Attribute function_declaration() {
		return new Attribute("DEFAULT_FUNCTION_DECLARATION")
			.foreground(style.scheme().foreground().base());
	}

	Attribute global_variable() {
		return new Attribute("DEFAULT_GLOBAL_VARIABLE")
			.foreground(palette.purple().darker(2))
			.bold();
	}

	public Attribute identifier() {
		return new Attribute("DEFAULT_IDENTIFIER")
			.foreground(style.scheme().foreground().base());
	}

	public Attribute instance_field() {
		return new Attribute("DEFAULT_INSTANCE_FIELD")
			.foreground(palette.purple().darker())
			.bold();
	}

	public Attribute keyword() {
		return new Attribute("DEFAULT_KEYWORD")
			.copy(commonKeyword());
	}

	Attribute label() {
		return new Attribute("DEFAULT_LABEL")
			.foreground(palette.blue()).bold();
	}

	public Attribute local_variable() {
		return new Attribute("DEFAULT_LOCAL_VARIABLE")
			.foreground(palette.purple());
	}

	public Attribute metadata() {
		return new Attribute("DEFAULT_METADATA")
			.foreground(palette.green());
	}

	Attribute number() {
		return new Attribute("DEFAULT_NUMBER")
			.foreground(palette.blue())
			.bold();
	}

	public Attribute parameter() {
		return new Attribute("DEFAULT_PARAMETER")
			.foreground(palette.purple())
			.bold();
	}

	Attribute predefined_symbol() {
		return new Attribute("DEFAULT_PREDEFINED_SYMBOL")
			.baseAttributes("DEFAULT_IDENTIFIER");
	}

	Attribute reassigned_local_variable() {
		return new Attribute("DEFAULT_REASSIGNED_LOCAL_VARIABLE")
			.foreground(palette.purple())
			.dottedLine(palette.purple());
	}

	Attribute reassigned_parameter() {
		return new Attribute("DEFAULT_REASSIGNED_PARAMETER")
			.foreground(palette.purple())
			.bold()
			.dottedLine(palette.purple());
	}

	Attribute static_field() {
		return new Attribute("DEFAULT_STATIC_FIELD")
			.foreground(palette.purple().darker()).bold()
			.italic();
	}

	Attribute static_method() {
		return new Attribute("DEFAULT_STATIC_METHOD")
			.foreground(style.scheme().foreground().base())
			.italic();
	}

	Attribute tag() {
		return new Attribute("DEFAULT_TAG")
			.foreground(palette.blue().darker());
	}

	Attribute template_language_color() {
		return new Attribute("DEFAULT_TEMPLATE_LANGUAGE_COLOR")
			.foreground(style.scheme().foreground().base().brighter(3));
	}

	public Attribute interface_name() {
		return new Attribute("DEFAULT_INTERFACE_NAME")
			.foreground(style.scheme().foreground().base())
			.italic();
	}
}
