package plugin.intellij.scheme.root.console_colors;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

class LogConsole extends Group {

	private final Style style;
	private final Palette palette;

	LogConsole(Style style, Palette palette) {
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			error_output(),
			expired_entry(),
			warning_output()
		);
	}

	Attribute error_output() {
		return new Attribute("LOG_ERROR_OUTPUT")
			.foreground(style.error());
	}

	Attribute expired_entry() {
		return new Attribute("LOG_EXPIRED_ENTRY")
			.foreground(palette.gray());
	}

	Attribute warning_output() {
		return new Attribute("LOG_WARNING_OUTPUT")
			.foreground(style.theme().warning());
	}
}
