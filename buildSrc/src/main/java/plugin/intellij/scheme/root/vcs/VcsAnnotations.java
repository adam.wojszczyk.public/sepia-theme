package plugin.intellij.scheme.root.vcs;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.color.Color;
import plugin.style.Style;

import java.util.Set;

class VcsAnnotations extends Group {

	private final Style style;

	VcsAnnotations(Style style) {
		this.style = style;
	}

	@Override
	public Set<Option.Color> colors() {
		Color freshestBackground = style.scheme().background().base();
		return Set.of(
			new Option.Color("VCS_ANNOTATIONS_COLOR_1", freshestBackground),
			new Option.Color("VCS_ANNOTATIONS_COLOR_2", freshestBackground.darker()),
			new Option.Color("VCS_ANNOTATIONS_COLOR_3", freshestBackground.darker(2)),
			new Option.Color("VCS_ANNOTATIONS_COLOR_4", freshestBackground.darker(3)),
			new Option.Color("VCS_ANNOTATIONS_COLOR_5", freshestBackground.darker(4)),
			foreground()
		);
	}
	private Option.Color foreground() {
		return new Option.Color("ANNOTATIONS_COLOR", style.scheme().foreground().base());
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of();
	}
}
