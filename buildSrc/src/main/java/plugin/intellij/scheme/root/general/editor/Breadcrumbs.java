package plugin.intellij.scheme.root.general.editor;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.style.Style;

import java.util.Set;

class Breadcrumbs extends Group {
	private final Style style;

	Breadcrumbs(Style style) {
		this.style = style;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			breadcrumbs_default(),
			hovered(),
			current(),
			inactive()
		);
	}

	Attribute breadcrumbs_default() {
		return new Attribute("BREADCRUMBS_DEFAULT")
			.foreground(style.scheme().foreground().base())
			.background(style.scheme().background().base());
	}

	Attribute hovered() {
		return new Attribute("BREADCRUMBS_HOVERED")
			.foreground(style.scheme().foreground().base())
			.background(style.scheme().background().base().darker(2));
	}

	Attribute current() {
		return new Attribute("BREADCRUMBS_CURRENT")
			.foreground(style.scheme().foreground().base())
			.background(style.scheme().background().base().darker(4));
	}

	Attribute inactive() {
		return new Attribute("BREADCRUMBS_INACTIVE")
			.foreground(style.scheme().foreground().base()); // unknown effect
	}
}
