package plugin.intellij.scheme.root.general;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.style.Style;

import java.util.Set;

import static java.util.Objects.requireNonNull;

class PopupsAndHints extends Group {

	private final Style style;

	public PopupsAndHints(Style style) {
		this.style = requireNonNull(style);
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of(
			new Option.Color("LOOKUP_COLOR", style.scheme().background().documentation()),
			new Option.Color("DOCUMENTATION_COLOR", style.scheme().background().documentation()),
			new Option.Color("ERROR_HINT", style.scheme().background().hints()),
			new Option.Color("INFORMATION_HINT", style.scheme().background().hints()),
			new Option.Color("PROMOTION_PANE", style.scheme().background().hints()),
			new Option.Color("QUESTION_HINT", style.scheme().background().hints()),
			new Option.Color("HINT_BORDER", style.theme().borderColor()),
			new Option.Color("RECENT_LOCATIONS_SELECTION", style.scheme().background().base().darker()),
			new Option.Color("TOOLTIP", style.scheme().background().hints())
		);
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(code_lens_border_color());
	}

	Attribute code_lens_border_color() {
		return new Attribute("CODE_LENS_BORDER_COLOR")
			.bordered(style.theme().borderColor());
	}
}
