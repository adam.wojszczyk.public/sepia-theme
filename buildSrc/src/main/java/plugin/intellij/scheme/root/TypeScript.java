package plugin.intellij.scheme.root;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

class TypeScript extends Group {
	private final Root root;
	private final Style style;
	private final Palette palette;

	TypeScript(Root root, Style style, Palette palette) {
		this.root = root;
		this.style = style;
		this.palette = palette;
	}

	@Override
	public Set<Option.Color> colors() {
		return Set.of();
	}

	@Override
	public Set<Attribute> attributes() {
		return Set.of(
			type_parameter(),
			type_guard()
		);
	}

	Attribute type_parameter() {
		return new Attribute("TS.TYPE_PARAMETER")
			.copy(root.java().type_parameter_name_attributes());
	}

	Attribute type_guard() {
		return new Attribute("TS.TYPE_GUARD")
			.emptyValue();
	}
}
