package plugin.intellij.scheme.root.console_colors;

import plugin.intellij.scheme.Attribute;
import plugin.intellij.scheme.Group;
import plugin.intellij.scheme.Option;
import plugin.model.Palette;
import plugin.style.Style;

import java.util.Set;

public class ConsoleColors extends Group {

	private final ANSIColors ansiColors;
	private final Console console;
	private final LogConsole logConsole;

	private final Group children;

	public ConsoleColors(Style style, Palette palette) {
		this.ansiColors = new ANSIColors(style, palette);
		this.console = new Console(style, palette);
		this.logConsole = new LogConsole(style, palette);
		this.children = new Group.Union(ansiColors, console, logConsole);
	}

	@Override
	public Set<Option.Color> colors() {
		return children.colors();
	}

	@Override
	public Set<Attribute> attributes() {
		return children.attributes();
	}

	ANSIColors ansiColors() { return ansiColors; }
	Console console() { return console; }
	LogConsole logConsole() { return logConsole; }
}
