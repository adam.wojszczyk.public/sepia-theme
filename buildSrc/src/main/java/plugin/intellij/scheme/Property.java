package plugin.intellij.scheme;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

import static java.util.Objects.requireNonNull;

class Property {

	private final String name;
	private final String text;

	Property(String name, String text) {
		this.name = requireNonNull(name);
		this.text = requireNonNull(text);
	}
	@JacksonXmlProperty(isAttribute = true) public String name() { return name; }
	@JacksonXmlText public String text() { return text; }
}
