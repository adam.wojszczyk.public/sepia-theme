package plugin.model.color;

import plugin.model.StringValue;

class Value extends StringValue {

	protected Value(String value) {
		super(value);
	}

	public String hex() {
		return "#" + string();
	}

	public String plain() {
		return string();
	}
}
